import Type from './_type.js'

export class Filter extends Type {
  get element () {
    return this.data.element
  }

  get modifier () {
    return this.data.modifier
  }

  get remote () {
    return this.data.remote
  }

  get local () {
    return this.data.local
  }

  get items () {
    let items = this.factory.wrap( this.data.items )
    if ( items.length ) {
      return items
    }
    return this.data.items
  }
}

export default Filter
