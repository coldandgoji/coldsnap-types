import Location from './location.js'

export class Office extends Location {
  get address () {
    return this.util.md( this.data.address )
  }

  get location () {
    return this.data.location
  }

  // ----------------------------------------------
  // Template
  // ----------------------------------------------

  get _container () {
    return `
      {{figure}}
      {{content}}
      {{meta}}`
  }

  get $figure () {
    if ( this.imageUrl || this.backgroundImageUrl ) {
      return `<figure class="office__figure" {{cssStyle}}>{{figure}}</figure>`
    }
  }

  get _figure () {
    return `
      {{image}}
      {{figureCaption}}`
  }

  get $image () {
    if ( this.imageUrl ) {
      return `<img class="office__image" src="${this.imageUrl}" alt="${this.imageDescription}" />`
      // ?w=1000&h=500
    }
  }

  get $figureCaption () {
    if ( this.data.caption ) {
      return `<figcaption class="office__caption">{{figureCaption}}</figcaption>`
    }
  }

  get _figureCaption () {
    return `${this.data.firstName} ${this.data.lastName}`
  }

  get $content () {
    return `<div class="office__content">{{content}}</div>`
  }

  get _content () {
    return `
      {{heading}}
      {{copy}}`
  }

  get $meta () {
    return `<div class="office__meta">{{meta}}</div>`
  }

  get _meta () {
    return `
      {{address}}
      {{map}}`
  }

  get $address () {
    return `<div class="office__address">{{address}}</div>`
  }

  get _address () {
    return this.address
  }

  get $map () {
    return `<div class="office__map" data-latitude="${this.location.lat}" data-longitude="${this.location.lon}"></div>`
  }
}

export default Office
