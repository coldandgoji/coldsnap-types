import ArticleListing from './_articleListing.js'

export class ArticleCategory extends ArticleListing {
  /**
   * Returns the url for this articleCategory
   * based on the provided section
   */
  url ( articleSection ) {
    return articleSection.url + '/categories/' + this.slug
  }

  /**
   * Retrieves all articles in this articleCategory
   * that are also within the specified articleSection
   */
  articles ( articleSection ) {
    return this.util._.filter(
      super.articles( articleSection ),
      article => {
        return article.categories.includes( this )
      }
    )
  }

  /**
   * Splits the articleCategory into 'pages'
   * This is performed 'per section' as a single articleCategory can
   * exist within multiple articleSections
   */
  pages ( perPage = 10 ) {
    // creates a holder for the resulting pages
    let pages = []

    // loops through all articleSections
    Object.values( this.ssg.data.articleSection ).forEach( articleSection => {
      // generates a set of pages
      pages = pages.concat(
        super.pages(
          articleSection,
          this.articles( articleSection ),
          this.url( articleSection ),
          perPage
        )
      )
    } )

    // returns the generated pages
    return pages
  }
}

export default ArticleCategory
