import Type from './_type.js'

export class Listing extends Type {
  get listingMode () {
    return ( this.data.remote ) ? 'remote' : 'local'
  }

  get entries () {
    return this.data.entries
  }

  get remote () {
    if ( this.listingMode === 'remote' ) {
      return {
        projectId: this.ssg.projectId,
        view: this.data.remote.view,
        locale: this.ssg.locale.remote,
        retrieve: this.jsonEncode( this.data.remote.retrieve ),
        filter: this.jsonEncode( this.data.remote.filter ),
        sort: this.jsonEncode( this.data.remote.sort ),
        sortForm: this.data.remote.sortForm,
        page: this.jsonEncode( this.data.remote.page )
      }
    }
    return {
      projectId: '',
      view: '',
      locale: '',
      retrieve: '',
      filter: '',
      sort: '',
      sortForm: '',
      page: ''
    }
  }

  /**
   * Returns the filters for this listing as form instances
   */
  get filters () {
    if ( !this.cachedFilters ) {
      let filters = []
      let form
      this.data.filters.forEach( ( filter ) => {
        if ( typeof filter === 'string' ) {
          form = this.form( filter )
        } else {
          form = this.form( filter.id, filter.element )
        }
        if ( form ) {
          filters.push( form )
        }
      } )
      this.cachedFilters = filters
    }
    return this.cachedFilters
  }

  /**
   * Generates a form instance to represent a filter
   * using the specified element type
   *
   * @param {id} The id of the filter to create the form for
   * @param {string} The string name of the element to represent it as (select, checkbox, text)
   */
  form ( id, element ) {
    // retrieves the filter object
    let filter = this.ssg.data.filter[ id ]

    if ( !filter ) {
      console.log( 'ERROR' )
      console.log( `The filter: '${id}' does not exist` )
      process.exit( 1 )
    }

    // starts building the form definition
    let form = {
      heading: filter.heading,
      modifiers: [ 'form--filter', `form--${this.util.slug( filter.heading )}` ],
      filter: this.jsonEncode( filter[ this.listingMode ] )
    }

    // uses the default filter element if none
    // was specified when requesting this form
    if ( !element && filter.element ) {
      element = filter.element
    }

    // creates a text field with submit button
    if ( element === 'search' ) {
      form.fields = [
        {
          type: 'text',
          name: this.util.slug( filter.id ),
          placeholder: ''
        },
        {
          type: 'submit',
          name: 'submit',
          placeholder: ''
        }
      ]

    // creates standard filter fields
    // checkboxes, radios, selects
    } else {
      // creates each field
      let options = filter.items.map( item => {
        let option = {
          id: this.util.slug( filter.id + '_' + item.id ),
          label: item.heading,
          value: item.value || item.id
        }

        if ( element === 'checkbox' || element === 'radio' ) {
          option.type = element
          option.name = this.util.slug( filter.id + '_' + item.id )
        }

        if ( item.filterParent ) {
          option.parent = item.filterParent
          option.label = ( element === 'select' ) ? '&nbsp;&nbsp;&nbsp;&nbsp;' + option.label : option.label
        }

        if ( item.protected ) {
          option.protected = true
        }

        return option
      } )

      // locates children within the set
      options = this.locateChildren( options )

      // culls filters that do not exist in the content for this listing
      options = this.cullFilters( filter, options )

      // determines how many choices are available
      let choices = []
      options.forEach( ( option ) => {
        if ( option.found === 'none' && !option.protected ) {
          option.disabled = true
        } else {
          choices.push( option )
        }
      } )

      // records if an initial item has been selected
      let selected = false

      // auto-selects the choice if there is only one available
      // and it is found against all the entries
      if ( choices.length === 1 && choices[ 0 ].found === 'all' ) {
        selected = true
        choices[ 0 ].disabled = true
        choices[ 0 ].selected = true
        choices = []
      }

      // disables the form if there is nothing active
      // or if there is only one result (meaning filtering is useless)
      if ( choices.length === 0 || this.entries.length === 1 ) {
        form.modifiers.push( 'form--disabled' )
      }

      // special handling for selects
      if ( element === 'select' ) {
        // inserts the default option for the select box
        let defaultOption = {
          id: this.util.slug( filter.id + '_default' ),
          label: 'All',
          value: ''
        }

        // selects the default option (as long as no other option is selected)
        if ( !selected ) {
          defaultOption.selected = true
        }

        // inserts the default option first
        options.unshift( defaultOption )

        // strips all disabled options
        // as long as they aren't the selected one
        // this has been added as a fix for an iOS issue
        // whereby it does not respect disabled 'option' values
        options = this.util._.filter(
          options,
          ( option ) => {
            return ( !option.disabled || option.selected )
          }
        )

        // creates the one select box field
        form.fields = [ {
          type: element,
          options: options
        } ]

      // stores the options as individual fields
      } else {
        form.fields = options
      }
    }

    // generate a custom form instance based on the runtime created data
    let result = ( form ) ? this.factory.generate( 'form', form ) : null

    return result
  }

  /**
   * Locates child options of each option within the set
   * by inspecting the parent property
   */
  locateChildren ( filterOptions ) {
    filterOptions.forEach( ( option ) => {
      option.children = filterOptions.reduce( ( result, child ) => {
        if ( child.parent === option.value ) {
          result.push( child.value )
        }
        return result
      }, [] )
    } )
    return filterOptions
  }

  /**
   * Culls a set of filter options
   * to only contain the ones that have at least
   * one matching entry within all the entries for this listing
   */
  cullFilters ( filter, filterOptions ) {
    // loops through every filterOption
    filterOptions.forEach( ( filterOption ) => {
      // retrieves the value for this filter
      // ensuring it's an array, so we can merge in child values
      let filterValues = filterOption.value
      if ( !Array.isArray( filterValues ) ) {
        filterValues = [ filterValues ]
      }

      // merges in any child values
      if ( filterOption.children ) {
        filterValues = filterValues.concat( filterOption.children )
      }

      // loops through the entries in this listing
      let foundCount = 0
      this.entries.forEach( ( entry ) => {
        // indicates if it was found on this entry
        let found = false

        // retrieves the values on the entry
        // for the filter we're inspecting
        let entryValues = entry.filters[ filter.local ]
        if ( !Array.isArray( entryValues ) ) {
          entryValues = [ entryValues ]
        }

        // handles an array of values on the entry
        for ( let entryValue of entryValues ) {
          for ( let filterValue of filterValues ) {
            if ( entryValue === filterValue ) {
              found = true
              break
            }
          }
        }

        // records that the value was found on this entry
        if ( found ) {
          foundCount++
        }
      } )

      // considers by default that the option
      // was found on at least some of the entries
      filterOption.found = 'some'

      // only reports usage when entries have been supplied
      // otherwise we must assume all filters are available
      if ( this.entries.length ) {
        // disables options that were found against
        // none of the entries
        if ( foundCount === 0 ) {
          filterOption.found = 'none'
        }

        // disables options that were found against
        // all of the entries
        if ( foundCount === this.entries.length ) {
          filterOption.found = 'all'
        }
      }
    } )

    return filterOptions
  }

  get $section () {
    if ( this.listingMode === 'remote' ) {
      return `
        <section class="listing listing--${this.listingMode}" data-listing-project-id="${this.remote.projectId}" data-listing-view="${this.remote.view}" data-listing-locale="${this.remote.locale}" data-listing-retrieve="${this.remote.retrieve}" data-listing-filter="${this.remote.filter}" data-listing-sort="${this.remote.sort}" data-listing-page="${this.remote.page}">
          {{container}}
        </section>`
    }
    return `
      <section class="listing listing--${this.listingMode}" data-listing-filter="${this.remote.filter}">
        {{container}}
      </section>`
  }

  get $container () {
    return `<div class="listing__container">{{container}}</div>`
  }

  get _container () {
    return `
      {{content}}
      {{filters}}
      {{main}}`
  }

  get $content () {
    return `<div class="listing__content">{{content}}</div>`
  }

  get _content () {
    return `
      {{heading}}
      {{copy}}`
  }

  get $heading () {
    return `<h3 class="listing__heading">{{heading}}</h3>`
  }

  get _heading () {
    return this.heading
  }

  get $copy () {
    return `<div class="listing__copy">{{copy}}</div>`
  }

  get _copy () {
    return this.copy
  }

  get $filters () {
    return `<div class="listing__filters">{{filters}}</div>`
  }

  get _filters () {
    return this.list( this.filters )
  }

  get $main () {
    return `<div class="listing__main">{{main}}</div>`
  }

  get _main () {
    return `
      {{sort}}
      {{results}}
    `
  }

  get $sort () {
    if ( this.remote.sortForm ) {
      return `
        <div class="listing__sort">
          {{sort}}
        </div>`
    }
  }

  get _sort () {
    return this.remote.sortForm
  }

  get $results () {
    return `<div class="listing__results" data-component="Listing">{{results}}</div>`
  }

  get _results () {
    if ( this.listingMode === 'local' ) {
      return `{{list}}`
    }
  }

  get $list () {
    return `<div class="listing__list">{{list}}</div>`
  }

  get _list () {
    if ( this.listingMode === 'local' ) {
      return '{{entries}}'
    }
  }

  get _entries () {
    return this.list( this.entries, 'summary' )
  }

  /*
  get _pagination () {
    return `
      <div class="listing__pagination">
        <section class="pagination">
          <div class="pagination__container">
            <ul class="pagination__list"></ul>
          </div>
        </section>
      </div>`
  }
  */
}

export default Listing
