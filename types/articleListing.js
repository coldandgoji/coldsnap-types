import Type from './_type.js'

export class ArticleListing extends Type {
  get articleSection () {
    return this.data.articleSection
  }

  get articles () {
    return this.data.articles
  }

  get url () {
    return this.data.url
  }

  // ------------------------------------------
  // Template
  // ------------------------------------------

  get _section () {
    return `
      {{banner}}
      {{container}}`
  }

  get $container () {
    // return this.div( 'container', '{{container}}' )
    return `<div class="article-listing__container">{{container}}</div>`
  }

  get _container () {
    return `
      {{main}}
      {{aside}}`
  }

  get $main () {
    // return this.div( 'main', '{{main}}' )
    return `<div class="article-listing__main">{{main}}</div>`
  }

  get _main () {
    return `
      {{collection}}
      {{pagination}} `
  }

  get $aside () {
    // return this.div( 'aside', '{{aside}}' )
    return `<div class="article-listing__aside">{{aside}}</div>`
  }

  get _aside () {
    return `
      {{articleSection}}
      {{articleCategories}}
      {{articleTags}}`
  }

  get $collection () {
    return this.component( 'collection.collection--articles', this._collection )
  }

  get _collection () {
    return {
      itemsMode: 'summary',
      items: this.articles
    }
  }

  get $pagination () {
    return this.data.pagination
  }

  get $articleSection () {
    if ( this.articleSection ) {
      return `<div class="article-listing__sections">{{articleSection}}</div>`
    }
  }

  get _articleSection () {
    if ( this.articleSection ) {
      return `<a class="article-listing__section" href="${this.articleSection.url}">${this.articleSection.heading}</a>`
    }
  }

  get $articleCategories () {
    if ( this.articleSection.categories.length ) {
      return `<div class="article-listing__categories">{{articleCategories}}</div>`
    }
  }

  get _articleCategories () {
    let html = ''
    this.articleSection.categories.forEach( category => {
      let isActive = ''
      if ( this.url.replace( /\/[0-9]+$/, '' ) === category.url( this.articleSection ) ) {
        isActive = ' is--active'
      }
      html += `<a class="article-listing__category${isActive}" href="${category.url( this.articleSection )}">${category.heading}</a>`
    } )
    return html
  }

  get $articleTags () {
    if ( this.articleSection.tags.length ) {
      return `<div class="article-listing__tags">{{articleTags}}</div>`
    }
  }

  get _articleTags () {
    let html = ''
    this.articleSection.tags.forEach( tag => {
      let isActive = ''
      if ( this.url.replace( /\/[0-9]+$/, '' ) === tag.url( this.articleSection ) ) {
        isActive = ' is--active'
      }
      html += `<a class="article-listing__tag${isActive}" href="${tag.url( this.articleSection )}">${tag.heading}</a>`
    } )
    return html
  }
}

export default ArticleListing
