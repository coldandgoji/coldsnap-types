import Type from './_type.js'

export class Banner extends Type {
  get links () {
    return this.factory.wrap( this.data.links, 'link' )
  }

  // ------------------------------------------
  // Template
  // ------------------------------------------

  get $section () {
    return `
      <section{{cssClass}}{{cssStyle}}{{dataId}}{{dataCaption}}>
        {{section}}
      </section>`
  }

  get $container () {
    return `<div class="banner__container"{{dataCaption}}>
      {{content}}
      {{actions}}
    </div>`
  }

  get $content () {
    return `<div class="banner__content">{{content}}</div>`
  }

  get _content () {
    return `
      {{heading}}
      {{copy}}`
  }

  get $actions () {
    if ( this.links && this.links.length ) {
      return `<div class="banner__actions">{{actions}}</div>`
    }
  }

  get _actions () {
    let html = ''
    this.links.forEach( link => {
      html += `<a class="banner__action" href="${link.url}">${link.text}</a>`
    } )
    return html
  }
}

export default Banner
