import Type from './_type.js'

export class _ArticleListing extends Type {
  constructor ( data, ssg ) {
    if ( new.target === _ArticleListing ) {
      throw new TypeError( 'Cannot create _ArticleListing instances directly' )
    }

    super( data, ssg )
  }

  /**
   * Retrieves all articles within an articleSection
   */
  articles ( articleSection ) {
    return this.sort( this.util._.filter(
      this.ssg.data.article,
      article => {
        return ( article.articleSection && article.articleSection.id === articleSection.id )
      }
    ) )
  }

  /**
   * Sorts a set of articles
   */
  sort ( articles ) {
    return this.util._.sortBy(
      articles,
      [
        function onSort ( article ) {
          return -( new Date( article.publicationDate ).getTime() )
        }
      ]
    )
  }

  /**
   * Splits the provided articles into ArticleListing 'pages'
   */
  pages ( articleSection, articles, url, perPage = 10 ) {
    // creates a holder for the resulting pages
    let pages = []

    // splits up the articles into perPage chunks
    let chunks = this.util._.chunk( articles, perPage )

    // determines the total number of pages
    let totalPages = chunks.length

    // creates an array of all available page links
    let linksAll = []
    for ( let i = 0; i < totalPages; i++ ) {
      linksAll.push( {
        index: i + 1,
        label: i + 1,
        url: ( i === 0 ) ? url : url + '/' + ( i + 1 )
      } )
    }

    // loops through the chunks
    chunks.forEach( ( chunk, index ) => {
      // determines the 'next' page
      let next = {
        label: 'Next',
        url: url + '/' + ( index + 2 )
      }
      if ( index === totalPages - 1 ) {
        next = null
      }

      // determines the 'previous' page
      let previous = {
        label: 'Previous',
        url: url + '/' + index
      }
      if ( index === 1 ) {
        previous.url = url
      }
      if ( index === 0 ) {
        previous = null
      }

      // available links
      let links = this.util._.cloneDeep( linksAll )
      links[ index ].active = true

      // creates a page
      pages.push(
        this.component( 'articleListing', {
          articleSection: articleSection,
          articles: chunk,
          url: ( index === 0 ) ? url : url + '/' + ( index + 1 ),
          pagination: this.component( 'pagination', {
            next: next,
            previous: previous,
            links: links
          } ),
          // contentType: this.contentType,
          heading: this.heading,
          image: this.image,
          banner: this.banner,
          metaTitle: this.metaTitle + ' - Page ' + ( index + 1 ),
          metaDescription: this.metaDescription
        } )
      )
    } )

    // returns the generated pages
    return pages
  }
}

export default _ArticleListing
