import Type from './_type.js'

export class ProductCategory extends Type {
  get url () {
    return '/products/' + this.data.slug
  }

  get products () {
    return this.factory.wrap( this.data.featured, 'product' )
  }

  get relatedProducts () {
    return this.factory.wrap( this.data.relatedProducts, 'product' )
  }
}

export default ProductCategory
