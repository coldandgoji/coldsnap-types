import Page from './page.js'

export class Faq extends Page {
  // ----------------------------------------------
  // Getters
  // ----------------------------------------------

  get url () {
    return '/faq/' + this.slug
  }

  get question () {
    return this.data.question
  }

  get answer () {
    return this.util.md( this.data.answer )
  }

  get slug () {
    return this.util.slug( this.util.striptags( this.question ) )
  }

  get metaTitle () {
    return this.util.striptags( this.util.md( this.data.question ) ).substr( 0, 60 )
  }

  get metaDescription () {
    return this.util.striptags( this.util.md( this.data.answer ) ).substr( 0, 160 )
  }

  get heading () {
    return this.question
  }

  get copy () {
    return this.answer
  }

  // ----------------------------------------------
  // Template
  // ----------------------------------------------

  get _heading () {
    return this.question
  }

  get _copy () {
    return this.answer
  }
}

export default Faq
