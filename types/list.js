import Type from './_type.js'

export class List extends Type {
  get items () {
    return this.cache( 'items', () => {
      return this.factory.wrap( this.data.items )
    } )
  }

  // ------------------------------------------
  // Output
  // ------------------------------------------
  get $section () {
    return this.list( this.items, this.data.itemsMode )
  }
}

export default List
