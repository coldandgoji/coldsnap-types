import Type from './_type.js'

export class ProductOption extends Type {
  get price () {
    return this.data.price
  }

  get priceFormatted () {
    return this.util.formatPrice( this.data.price )
  }
}

export default ProductOption
