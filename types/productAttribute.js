import Type from './_type.js'

export class ProductAttribute extends Type {
  get parentAttribute () {
    if ( this.data.reference_attribute && this.data.reference_attribute[ 0 ] ) {
      return this.factory.make( this.data.reference_attribute[ 0 ], 'attribute' )
    }
  }

  get url () {
    return this.cache( 'url', () => {
      let url = '/products'
      if ( this.parentAttribute ) {
        url += '/' + this.parentAttribute.slug
      }
      url += '/' + this.slug
      return url
    } )
  }
}

export default ProductAttribute
