import Type from './_type.js'

export class ProductLine extends Type {
  get url () {
    return this.cache( 'url', () => {
      let url = '/shop'
      if ( this.brand ) {
        url += '/' + this.brand.slug
      }
      url += '/' + this.slug
      return url
    } )
  }

  get weight () {
    return this.data.weight
  }

  get height () {
    return this.data.height
  }

  get width () {
    return this.data.width
  }

  get length () {
    return this.data.length
  }

  // TODO: Deprecate this
  // adds the 'pattern' value
  // this is temporary to mimic the existing site
  // as it uses pattern to distinguish between
  // products and productLines
  // this could now be performed by checking `partial.contentType`
  get pattern () {
    return 'line'
  }

  get copy () {
    return this.util.md( this.data.longDescription || '' )
  }

  get tags () {
    return this.data.tags
  }

  get price () {
    return this.data.price
  }

  get priceFormatted () {
    return this.cache( 'priceFormatted', () => {
      return this.util.formatPrice( this.data.price )
    } )
  }

  get salePrice () {
    return this.cache( 'salePrice', () => {
      let salePrice = this.data.salePrice
      if ( this.onSale && parseFloat( salePrice ) < parseFloat( this.price ) ) {
        return salePrice
      }
      return null
    } )
  }

  get salePriceFormatted () {
    return this.cache( 'salePriceFormatted', () => {
      return this.util.formatPrice( this.salePrice )
    } )
  }

  get finalPrice () {
    if ( this.salePrice ) {
      return this.salePrice
    }
    return this.price
  }

  get finalPriceFormatted () {
    if ( this.salePrice ) {
      return this.salePriceFormatted
    }
    return this.priceFormatted
  }

  get onSale () {
    return this.cache( 'onSale', () => {
      if ( this.attributeIds && this.attributeIds.onSaleYes && this.data.attributes && this.data.attributes.indexOf( this.attributeIds.onSaleYes ) !== -1 ) {
        return true
      }
      return false
    } )
  }

  get outOfStock () {
    return this.cache( 'outOfStock', () => {
      if ( this.attributeIds && this.attributeIds.outOfStock && this.data.attributes ) {
        return ( this.data.attributes.indexOf( this.attributeIds.outOfStock ) !== -1 )
      }
      return false
    } )
  }

  get brand () {
    let brand = this.backref( 'brand', 'allProductItems' )
    if ( brand && brand.length ) {
      return brand[ 0 ]
    }
    return null
  }

  get products () {
    return this.factory.wrap( this.data.products, 'product' )
  }

  get relatedProducts () {
    return this.factory.wrap( this.data.relatedProducts, 'product' )
  }

  get metaTitle () {
    return this.cache( 'metaTitle', () => {
      let metaTitle = ''

      if ( this.data.name ) {
        metaTitle += this.data.name
      }

      if ( this.brand && this.brand.name ) {
        if ( metaTitle !== '' ) {
          metaTitle += ' - '
        }
        metaTitle += this.brand.name
      }

      return metaTitle
    } )
  }

  get metaDescription () {
    return this.cache( 'metaDescription', () => {
      if ( this.data.longDescription ) {
        return this.data.longDescription
      }
      return this.site.description
    } )
  }

  // ------------------------------------------
  // Template
  // ------------------------------------------

  get _container () {
    if ( this.$mode === 'summary' ) {
      return `
        {{image}}
        {{content}}`
    }
    return `
      {{content}}
      {{products}}
      {{relatedProducts}}`
  }

  get $image () {
    if ( this.imageUrl ) {
      return `<div class="product-line__image">{{image}}</div>`
    }
  }

  get _image () {
    return `<img class="image" src="${this.imageUrl}" alt="">`
  }

  get $content () {
    return `<div class="product-line__content">{{content}}</div>`
  }

  get _content () {
    return `
      {{tags}}
      {{name}}
      {{prices}}
      {{summary}}
      {{actions}}`
  }

  get _tags () {
    let html = ''
    if ( this.tags ) {
      html += '<ul class="product-line__tags">'
      this.tags.forEach( tag => {
        html += `<li class="product-line__tag product-line__tag--${this.util.slug( tag )}">${tag}</li>`
      } )
      html += '</ul>'
    }
    return html
  }

  get $name () {
    if ( this.name ) {
      return `<div class="product-line__name">{{name}}</div>`
    }
  }

  get _name () {
    return this.name
  }

  get _prices () {
    let html = ''
    if ( this.salePrice || this.price ) {
      html = '<div class="product-line__prices">'
      if ( this.salePrice ) {
        html += `
        <span class="product-line__price product-line__price--original-price">${this.priceFormatted}</span>
        <span class="product-line__price product-line__price--sale-price">${this.salePriceFormatted}</span>
      `
      } else {
        html += `<span class="product-line__price">${this.priceFormatted}</span>`
      }
      html += '</div>'
    }
    return html
  }

  get _actions () {
    let html = '<div class="product-line__actions">'
    html += `<a class="product-line__button product-line__button--more-info"
    href="${this.url}">Learn more</a>`
    html += '</div>'
    return html
  }

  get _products () {
    if ( this.products.length ) {
      return this.component( 'collection.collection--products', {
        heading: 'Products',
        itemsMode: 'summary',
        items: this.products
      } )
    }
  }

  // lists the related products
  get _relatedProducts () {
    return this.component( 'collection.collection--products.collection--products-related', {
      heading: 'Related Products',
      itemsMode: 'summary',
      items: this.relatedProducts
    } )
  }

  get _link () {
    if ( this.url ) {
      return `<a class="product-line__link" href="${this.url}"></a>`
    }
  }
}

export default ProductLine
