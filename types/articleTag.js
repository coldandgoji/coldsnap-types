import ArticleListing from './_articleListing.js'

export class ArticleTag extends ArticleListing {
  /**
   * Returns the url for this articleTag
   * based on the provided section
   */
  url ( articleSection ) {
    return articleSection.url + '/tags/' + this.slug
  }

  /**
   * Retrieves all articles with this articleTag
   * that are also within the specified articleSection
   */
  articles ( articleSection ) {
    return this.util._.filter(
      super.articles( articleSection ),
      article => {
        return article.tags.includes( this )
      }
    )
  }

  /**
   * Splits the articleTag into 'pages'
   * This is performed 'per section' as a single articleTag can
   * exist within multiple articleSections
   */
  pages ( perPage = 10 ) {
    // creates a holder for the resulting pages
    let pages = []

    // loops through all articleSections
    Object.values( this.ssg.data.articleSection ).forEach( articleSection => {
      // generates a set of pages
      pages = pages.concat(
        super.pages(
          articleSection,
          this.articles( articleSection ),
          this.url( articleSection ),
          perPage
        )
      )
    } )

    // returns the generated pages
    return pages
  }
}

export default ArticleTag
