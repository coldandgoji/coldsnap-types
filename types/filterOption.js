import Type from './_type.js'

export class FilterOption extends Type {
  get element () {
    return this.data.element
  }

  get value () {
    return this.data.value
  }

  get protected () {
    return this.data.protected
  }

  get filterParent () {
    return this.data.filterParent
  }
}

export default FilterOption
