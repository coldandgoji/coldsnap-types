import ArticleListing from './_articleListing.js'

export class ArticleSection extends ArticleListing {
  /**
   * Returns the url for this articleSection
   */
  get url () {
    return '/articles/' + this.slug
  }

  /**
   * Retrieves all articles in this articleSection
   */
  get articles () {
    return super.articles( this )
  }

  /**
   * Splits the articleSection into 'pages'
   */
  pages ( perPage = 10 ) {
    return super.pages(
      this,
      this.articles,
      this.url,
      perPage
    )
  }

  /**
   * Retrieves all categories used by articles in this articleSection
   */
  get categories () {
    return this.cache( 'categories', () => {
      let categories = []
      this.articles.forEach( article => {
        article.categories.forEach( category => {
          if ( !categories.includes( category ) ) {
            categories.push( category )
          }
        } )
      } )
      return categories
    } )
  }

  /**
   * Retrieves all tags used by articles in this articleSection
   */
  get tags () {
    return this.cache( 'tags', () => {
      let tags = []
      this.articles.forEach( article => {
        article.tags.forEach( tag => {
          if ( !tags.includes( tag ) ) {
            tags.push( tag )
          }
        } )
      } )
      return tags
    } )
  }
}

export default ArticleSection
