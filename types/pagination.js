import Type from './_type.js'

export class Pagination extends Type {
  get _container () {
    return `
      <ul class="pagination__list">
        {{previous}}
        {{links}}
        {{next}}
      </ul>`
  }

  get _previous () {
    let cssClass = ( this.data.previous ) ? '' : ' is--disabled'
    let href = ( this.data.previous ) ? this.data.previous.url : 'javascript:void(0);'
    return `
    <li class="pagination__item pagination__item--previous${cssClass}">
      <a class="pagination__link" href="${href}">Previous</a>
    </li>`
  }

  get _links () {
    let html = ''
    let isActive
    this.data.links.forEach( ( link, index ) => {
      isActive = ( link.active ) ? 'is--active' : ''
      html += `<li class="pagination__item pagination__item--${index} ${isActive}"><a class="pagination__link" href="${link.url}">${link.label}</a></li>`
    } )
    return html
  }

  get _next () {
    let cssClass = ( this.data.next ) ? '' : ' is--disabled'
    let href = ( this.data.next ) ? this.data.next.url : 'javascript:void(0);'
    return `
    <li class="pagination__item pagination__item--next${cssClass}">
      <a class="pagination__link" href="${href}">Next</a>
    </li>`
  }
}

export default Pagination
