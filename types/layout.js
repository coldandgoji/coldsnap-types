import Type from './_type.js'

export class Layout extends Type {
  get _container () {
    return `
      <div class="layout__items">
        ${this.list( this.factory.wrap( this.data.items ), this.data.itemsMode )}
      </div>`
  }
}

export default Layout
