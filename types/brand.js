import Type from './_type.js'

export class Brand extends Type {
  get url () {
    return '/shop/' + this.slug
  }

  get copy () {
    return this.data.copy
  }

  get allProductItems () {
    return this.factory.wrap( this.data.productLines )
  }

  get productLines () {
    return this.factory.wrap( this.data.productLines, 'productLine' )
  }

  get products () {
    return this.factory.wrap( this.data.productLines, 'product' )
  }
}

export default Brand
