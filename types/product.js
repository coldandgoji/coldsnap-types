import Type from './_type.js'

export class Product extends Type {
  get sku () {
    return this.data.sku
  }

  get url () {
    return this.cache( 'url', () => {
      let url = '/products'
      if ( this.brand ) {
        url += '/' + this.brand.slug
      }
      if ( this.productLine ) {
        url += '/' + this.productLine.slug
      }
      url += '/' + this.slug
      return url
    } )
  }

  get inventoryType () {
    return this.data.inventoryType ?? ( this.addon ? 'addon' : 'product' )
  }

  get inventoryCategories () {
    return this.data.categories
  }

  get inventoryDescription () {
    return this.data.inventoryDescription
  }

  get inventoryGroup () {
    return this.data.inventoryGroup
  }

  get inventoryFields () {
    return this.data.inventoryFields
  }

  get inventoryAttributes () {
    return this.data.inventoryAttributes
  }

  get inventoryProducts () {
    return this.data.inventoryProducts
  }

  get inventoryStock () {
    return this.data.inventoryStock
  }

  get inventoryDigital () {
    return !!( this.data.inventoryDigital )
  }

  get inventoryAvailable () {
    return !!( this.finalPrice !== undefined && !this.outOfStock )
  }

  get inventoryQuantity () {
    return this.data.inventoryQuantity
  }

  get inventoryConfigurable () {
    return this.data.inventoryConfigurable
  }

  get inventoryBalance () {
    return this.data.inventoryBalance
  }

  get measure () {
    return this.data.measure
  }

  get description () {
    return this.cache( 'description', () => {
      return this.util.md( this.data.description )
    } )
  }

  get maxQuantity () {
    return this.data.maxQuantity
  }

  get image () {
    return this.cache( 'image', () => {
      if ( super.image ) {
        return super.image
      }
      if ( this.productLine && this.productLine.image ) {
        return this.productLine.image
      }
      return ''
    } )
  }

  get images () {
    return this.cache( 'images', () => {
      // returns the images for this product
      let images = super.images
      if ( images && images.length ) {
        return images
      }

      // returns the images for the productLine
      if ( this.productLine ) {
        images = this.productLine.images
        if ( images && images.length ) {
          return images
        }
      }

      // returns no images
      return []
    } )
  }

  // exposes and formats the price
  get price () {
    return this.cache( 'price', () => {
      if ( !this.data.price && this.productLine && this.productLine.price ) {
        return this.productLine.price
      }
      return this.data.price
    } )
  }

  get priceFormatted () {
    return this.util.formatPrice( this.price )
  }

  get salePrice () {
    return this.cache( 'salePrice', () => {
      // determines the salePrice
      // either directly or via the productLine
      let salePrice = this.data.salePrice
      if ( salePrice === undefined && this.productLine && this.productLine.salePrice ) {
        salePrice = this.productLine.salePrice
      }

      // returns the salePrice only when actually 'on sale'
      if ( this.onSale && parseFloat( salePrice ) < parseFloat( this.price ) ) {
        return salePrice
      }

      // no sale price found for this product
      return undefined
    } )
  }

  get salePriceFormatted () {
    return this.util.formatPrice( this.salePrice )
  }

  get finalPrice () {
    if ( this.salePrice !== undefined ) {
      return this.salePrice
    }
    return this.price
  }

  get finalPriceFormatted () {
    if ( this.salePrice !== undefined ) {
      return this.salePriceFormatted
    }
    return this.priceFormatted
  }

  get onSale () {
    return this.cache( 'onSale', () => {
      // checks if the product is directly 'onSale'
      if ( this.attributeIds && this.attributeIds.onSaleYes && this.data.attributes && this.data.attributes.indexOf( this.attributeIds.onSaleYes ) !== -1 ) {
        return true
      }

      // checks if the product is specifically NOT 'onSale'
      if ( this.attributeIds && this.attributeIds.onSaleNo && this.data.attributes && this.data.attributes.indexOf( this.attributeIds.onSaleNo ) !== -1 ) {
        return false
      }

      // falls back to using the productLine 'onSale' value
      if ( this.productLine ) {
        return this.productLine.onSale
      }

      // assumes that the product is not 'onSale'
      return false
    } )
  }

  get outOfStock () {
    return this.cache( 'outOfStock', () => {
      if ( this.attributeIds && this.attributeIds.outOfStock && this.data.attributes ) {
        return ( this.data.attributes.indexOf( this.attributeIds.outOfStock ) !== -1 )
      }
      return false
    } )
  }

  get addon () {
    return this.cache( 'addon', () => {
      if ( this.attributeIds && this.attributeIds.addon && this.data.attributes ) {
        return ( this.data.attributes.indexOf( this.attributeIds.addon ) !== -1 )
      }
      return false
    } )
  }

  get weight () {
    return this.cache( 'weight', () => {
      if ( !this.data.weight && this.productLine && this.productLine.weight ) {
        return this.productLine.weight
      }
      return this.data.weight
    } )
  }

  get height () {
    return this.cache( 'height', () => {
      if ( !this.data.height && this.productLine && this.productLine.height ) {
        return this.productLine.height
      }
      return this.data.height
    } )
  }

  get width () {
    return this.cache( 'width', () => {
      if ( !this.data.width && this.productLine && this.productLine.width ) {
        return this.productLine.width
      }
      return this.data.width
    } )
  }

  get length () {
    return this.cache( 'length', () => {
      if ( !this.data.length && this.productLine && this.productLine.length ) {
        return this.productLine.length
      }
      return this.data.length
    } )
  }

  get categories () {
    return this.factory.wrap( this.data.categories, 'productCategory' )
  }

  get brand () {
    return this.cache( 'brand', () => {
      // this product is directly associated with a brand
      let brand = this.backref( 'brand', 'allProductItems' )
      if ( brand && brand.length ) {
        return brand[ 0 ]
      }

      // this product inherits its brand from a productLine
      if ( this.productLine ) {
        return this.productLine.brand
      }

      // this product does not have a brand
      return null
    } )
  }

  get productLine () {
    return this.cache( 'productLine', () => {
      let productLine = this.backref( 'productLine', 'products' )
      if ( productLine && productLine.length ) {
        return productLine[ 0 ]
      }
      return null
    } )
  }

  get relatedProducts () {
    return this.factory.wrap( this.data.relatedProducts, 'product' )
  }

  get options () {
    return this.factory.wrap( this.data.options, 'product' )
  }

  get copy () {
    return this.cache( 'copy', () => {
      let copy = this.util.md( this.data.longDescription || '' )
      if ( !copy && this.productLine ) {
        copy = this.productLine.copy
      }
      return copy
    } )
  }

  get facebookShareUrl () {
    return this.util.facebookShareUrl( this.ssg.locale.domain + this.url, 'http:' + this.image.url )
  }

  get pinterestShareUrl () {
    return this.util.pinterestShareUrl( this.ssg.locale.domain + this.url, 'http:' + this.image.url, this.description )
  }

  get tags () {
    return this.data.tags
  }

  /*
  get carouselImages () {
    let carouselImages = []

    // adds the images from this product
    let images = this.data.images || []
    images.forEach( ( image ) => {
      carouselImages.push(
        {
          id: this.data.id,
          image: image
        }
      )
    } )

    // adds additional productLine images
    if ( this.productLine ) {
      // adds the images from the productLine
      let productLineImages = this.productLine.images || []
      productLineImages.forEach( ( image ) => {
        carouselImages.push(
          {
            id: this.productLine.id,
            image: image
          }
        )
      } )

      // adds the images from the productLine products
      let products = this.productLine.products || []
      products.forEach( ( product ) => {
        if ( product.images && product.price > 0 ) {
          product.images.forEach( ( image ) => {
            carouselImages.push( {
              id: product.id,
              image: image
            } )
          } )
        }
      } )
    }

    // builds the carousel html
    let html = ''
    carouselImages.forEach( ( image ) => {
      html += '<li class="carousel__item"'
      if ( image.id ) {
        html += ` data-option-value="${image.id}"`
      }
      html += '>'
      html += '<figure class="carousel__figure"'
      if ( image.id ) {
        html += ` value="${image.id}"`
      }
      html += ` style="background-image:url(${image.image.url}?w=500&h=500);"`
      html += ' />'
      html += '</li>'
    } )

    // returns the built html
    return html
  }
  */

  /*
  get form () {
    // determines the heading
    let heading = this.name
    if ( this.brand ) {
      heading = `${this.brand.name} - ${this.name}`
    }
    if ( this.brand && this.productLine ) {
      heading = `${this.brand.name} - ${this.productLine.name}`
    }

    // determines the copy
    let copy = '<h6>'
    copy += '<span itemprop="priceCurrency" content="AUD" />'
    copy += `<span itemprop="price" content="${this.priceFormatted}">${this.priceFormatted}</span>`
    copy += '</h6>'
    copy += `<p itemprop="description">${this.copy}</p>`

    // determines the product fields
    let fields = []

    if ( this.productLine && this.type ) {
      // *sigh*
    }

    let formData = {
      modifier: 'product',
      heading: heading,
      copy: copy,
      fields: fields
    }

    // generate a custom form instance based on the runtime created data
    return this.factory.generate( 'form', formData )
  }
  */

  // ------------------------------------------
  // Template
  // ------------------------------------------

  get _container () {
    return `
      {{image}}
      {{content}}`
  }

  get $image () {
    if ( this.imageUrl ) {
      return `<div class="product__image">{{image}}</div>`
    }
  }

  get _image () {
    return `<img class="image" src="${this.imageUrl}" alt="">`
  }

  get $content () {
    return `<div class="product__content">{{content}}</div>`
  }

  get _content () {
    return `
      {{tags}}
      {{name}}
      {{prices}}
      {{summary}}
      {{actions}}`
  }

  get _tags () {
    let html = ''
    if ( this.tags ) {
      html += '<ul class="product__tags">'
      this.tags.forEach( tag => {
        html += `<li class="product__tag product__tag--${this.util.slug( tag )}">${tag}</li>`
      } )
      html += '</ul>'
    }
    return html
  }

  get $name () {
    if ( this.name ) {
      return `<div class="product__name">{{name}}</div>`
    }
  }

  get _name () {
    return this.name
  }

  get _prices () {
    let html = ''
    if ( this.salePrice !== undefined || this.price !== undefined ) {
      html = '<div class="product__prices">'
      if ( this.salePrice !== undefined ) {
        html += `
        <span class="product__price product__price--original-price">${this.priceFormatted}</span>
        <span class="product__price product__price--sale-price">${this.salePriceFormatted}</span>
      `
      } else {
        html += `<span class="product__price">${this.priceFormatted}</span>`
      }
      html += '</div>'
    }
    return html
  }

  get _addOns () {
    if ( this.price > 0 && !this.outOfStock ) {
      if ( this.options.length ) {
        let html = ''
        html += '<div class="options">'
        html += '<div class="options__heading">Add-Ons</div>'
        this.options.forEach( option => {
          html += `<div class="option" data-item-id="${option.id}">`
          html += '<div class="option__container">'
          html += '<div class="option__checkbox">'
          html += `<input id="${option.id}" type="checkbox" data-item-id="${option.id}" />`
          html += '</div>'
          html += '<div class="option__image">'
          html += `<img src="${option.image.url}?w=50&h=50&fit=pad&q=80">`
          html += '</div>'
          html += '<div class="option__summary">'
          html += option.summary
          html += '</div>'
          html += '</div>'
          html += '<div class="option__overlay"></div>'
          html += '</div>'
        } )
        html += '</div>'
        return html
      }
    }
  }

  get _actions () {
    let html = '<div class="product__actions">'
    html += `<a class="product__button product__button--more-info"
    href="${this.url}">Learn more</a>`
    if ( this.pattern !== 'line' && this.price ) {
      html += '{{cartButton}}'
    }
    html += '</div>'
    return html
  }

  get _cartButton () {
    let html = '<a href="#" class="product__button product__button--add-to-cart is--disabled">Add to cart</a>'
    if ( this.price > 0 && !this.outOfStock ) {
      html = '<a href="#" class="product__button product__button--add-to-cart" '
      html += `data-item-id="${this.id}" `
      html += '>' + 'Add to cart' + '</a>'
    }
    return html
  }

  get _link () {
    if ( this.url ) {
      return `<a class="product__link" href="${this.url}"></a>`
    }
  }
}

export default Product
