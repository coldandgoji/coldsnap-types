import Type from './_type.js'

export class Content extends Type {
  get links () {
    return this.factory.wrap( this.data.links, 'link' )
  }

  get link () {
    return this.cache( 'link', () => {
      let link = null
      if ( this.data.link ) {
        link = this.factory.make( this.data.link )
        if ( !link ) {
          link = this.data.link
        }
      }
      if ( !link && this.links ) {
        link = this.util._.get( this.links, '[0]', '' )
      }
      return link
    } )
  }

  // ----------------------------------------------
  // Template
  // ----------------------------------------------

  get _container () {
    return `
      {{figure}}
      {{content}}
      {{link}}`
  }

  get $figure () {
    if ( this.imageUrl || this.backgroundImageUrl ) {
      return `<figure class="content__figure" {{cssStyle}}>{{figure}}</figure>`
    }
  }

  get _figure () {
    return `
      {{image}}
      {{figureCaption}}`
  }

  get _image () {
    if ( this.imageUrl ) {
      return `<img class="content__image" src="${this.imageUrl}"{{alt}}/>`
    }
  }

  get _alt () {
    if ( this.imageDescription ) {
      return ` alt="${this.imageDescription}"`
    }
  }

  get $figureCaption () {
    if ( this.data.caption ) {
      return `<figcaption class="content__caption">{{figureCaption}}</figcaption>`
    }
  }

  get _figureCaption () {
    return this.data.caption
  }

  get $content () {
    if ( this._heading || this._summary || this._copy || this._actions ) {
      return `<div class="content__content">{{content}}</div>`
    }
  }

  get _content () {
    return `
      {{heading}}
      {{summary}}
      {{copy}}
      {{actions}}`
  }

  get $actions () {
    if ( this.links && this.links.length ) {
      return `<div class="content__actions">{{actions}}</div>`
    }
  }

  get _actions () {
    let html = ''
    this.links.forEach( link => {
      let external = link.external ? ' target="_blank" rel="noopener"' : ''
      html += `<a class="content__action" href="${link.url}"${external}>${link.text}</a>`
    } )
    return html
  }

  get _link () {
    if ( this.link && this.link.url ) {
      let external = this.link.external ? ' target="_blank" rel="noopener"' : ''
      return `<a class="content__link" href="${this.link.url}"${external}></a>`
    }
  }
}

export default Content
