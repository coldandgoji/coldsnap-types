import Type from './_type.js'

export class Toggle extends Type {
  get activate () {
    return this.data.activate
  }

  get deactivate () {
    return this.data.deactivate
  }

  get toggle () {
    return this.data.toggle
  }

  get label () {
    return this.data.label
  }

  get labelToggled () {
    return this.data.labelToggled
  }

  // ------------------------------------------
  // Template
  // ------------------------------------------
  get $section () {
    return `
      <div {{cssClass}} data-toggle="${this.toggle}" data-label="${this.label}" data-label-toggled="${this.labelToggled}">
        <div class="toggle__container">
          <button class="toggle__button" aria-pressed="false">
            <span class="toggle__icon"></span>
            <span class="toggle__label">${this.label}</span>
          </button>
        </div>
      </div>`
  }
}

export default Toggle
