export default class Mode {
  constructor ( data, ssg ) {
    if ( new.target === Mode ) {
      throw new TypeError( 'Cannot create Mode instances directly' )
    }

    // provides convience access
    // to global helpers
    this.ssg = ssg

    // creates the empty holder for flattened modes
    this._modes = {}

    // creates the handlebars regex
    this.handlebars = new RegExp( /{{[a-zA-Z0-9-]+}}/g )

    // alias this.factory.component for convenience
    this.component = ssg.factory.component.bind( ssg.factory )
  }

  get factory () {
    return this.ssg.factory
  }

  get site () {
    return this.ssg.site
  }

  get util () {
    return this.ssg.util
  }

  // ------------------------------------------
  // Override in Subclass
  // ------------------------------------------

  get $section () {
    return `
      <section{{cssClass}}{{dataId}}{{htmlId}}>
        {{section}}
      </section>`
  }

  get _section () {
    if ( this.$mode === 'summary' ) {
      return `
        {{container}}
        {{link}}`
    }
    return `{{container}}`
  }

  get $container () {
    return `<div class="${this.cssContentType}__container">{{container}}</div>`
  }

  get _container () {
    return ''
  }

  get _cssClass () {
    return ' class="' + this.modifiers.join( ' ' ) + '"'
  }

  get _dataId () {
    if ( this.id ) {
      return ` data-id="${this.id}"`
    }
  }

  get _htmlId () {
    if ( this.data.htmlId ) {
      return ` id="${this.data.htmlId}"`
    }
  }

  // ------------------------------------------
  // Public
  // ------------------------------------------
  output ( mode = 'page' ) {
    // stores the mode currently being processed
    // so that we can do conditionals in sub-elements
    // based on the currently processing mode
    this.$mode = mode

    // processes the mode for the first time
    // if ( !this._modes[ mode ] ) {

    // retrieves the base section
    let value = this.$section || ''
    value = ( value.output ) ? value.output() : value
    if ( typeof value !== 'string' ) {
      this.ssg.log( '' )
      this.ssg.log( 'ERROR' )
      this.ssg.log( '$section did not return a string' )
      this.ssg.log( `type: ${ this.contentType }` )
      this.ssg.log( `mode: ${ mode }` )
      this.ssg.log( `$section: ${ this.$section }` )
      this.ssg.log( '' )
      process.exit( 1 )
    }

    // flattens the section and caches the result
    // when it contains handlebars to replace
    if ( value.indexOf( '{{' ) !== -1 ) {
      this._modes[ mode ] = this.replaceHandlebars( value, '|section|', 0 )

      // just caches the section directly
      // when it does not contain handlebars
    } else {
      this._modes[ mode ] = value
    }
    // }

    // returns the flattened mode
    return this._modes[ mode ]
  }

  list ( items, mode ) {
    let html = ''
    if ( items && items.length ) {
      items.forEach( item => {
        if ( item.output ) {
          html += item.output( mode )
        } else {
          html += item
        }
      } )
    }
    return html
  }

  // ------------------------------------------
  // Private
  // ------------------------------------------

  isGetter ( object, property ) {
    let descriptor
    do {
      descriptor = Object.getOwnPropertyDescriptor( object, property )
    } while ( !descriptor && ( object = Object.getPrototypeOf( object ) ) )
    if ( descriptor ) {
      return !!descriptor['get']
    }
    return false
  }

  replaceHandlebars ( string, elements, depth ) {
    if ( depth === 20 ) {
      this.ssg.log( '' )
      this.ssg.log( 'ERROR: Possible infinite recursion detected' )
      this.ssg.log( '--------------------------------' )
      this.ssg.log( `String: ${ string }` )
      this.ssg.log( `Depth: ${ depth }` )
      this.ssg.log( '--------------------------------' )
      process.exit( 1 )
    }
    if ( typeof string !== 'string' ) {
      this.ssg.log( '' )
      this.ssg.log( 'ERROR: Expected string value is not a string' )
      this.ssg.log( '--------------------------------' )
      this.ssg.log( `String: ${ string }` )
      this.ssg.log( `Depth: ${ depth }` )
      this.ssg.log( '--------------------------------' )
      process.exit( 1 )
    }
    let recurse = false
    let matches = this.util._.uniq( string.match( this.handlebars ) )
    matches.forEach( token => {
      let name = this.util._.camelCase( token.replace( /[{}]/g, '' ) )
      let getter = ''
      let value = ''
      if ( elements.indexOf( '|' + name + '|' ) === -1 && this.isGetter( this, '$' + name ) ) {
        getter = '$' + name
        elements += '|' + name + '|'
      } else {
        getter = '_' + name
      }
      value = this[ getter ] || ''
      if ( Array.isArray( value ) ) {
        value = this.component( 'list', { items: value } )
      }
      value = ( value.output ) ? value.output() : value
      if ( typeof value !== 'string' ) {
        this.ssg.log( '' )
        this.ssg.log( 'ERROR: Expected string value is not a string' )
        this.ssg.log( 'You may have forgotten to add \'get\' to a getter' )
        this.ssg.log( '--------------------------------' )
        this.ssg.log( `Getter: ${ getter }` )
        this.ssg.log( `String: ${ value }` )
        this.ssg.log( `Depth: ${ depth }` )
        this.ssg.log( '--------------------------------' )
        process.exit( 1 )
      }
      if ( value.indexOf( '{{' ) !== -1 ) {
        recurse = true
      }
      string = string.replace( new RegExp( token, 'g' ), value )
    } )
    if ( recurse ) {
      string = this.replaceHandlebars( string, elements, depth + 1 )
    }
    return string
  }
}
