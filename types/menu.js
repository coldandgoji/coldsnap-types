import Type from './_type.js'

export class Menu extends Type {
  isActive ( item ) {
    if ( this.ssg.item.url === item.url ) {
      return ' is--active'
    }
    return ''
  }

  /**
   * Retrieves the items in this menu
   * These can be a mix of anything that has a `.url`,
   * most often they will be 'Link' or 'Page'
   */
  get items () {
    return this.factory.wrap( this.data.items )
  }

  // ------------------------------------------
  // Output
  // ------------------------------------------
  get $section () {
    return this.outputMenu( 'menu', this.items )
  }

  outputMenu ( depth, items ) {
    let cssClass = [ depth ]

    if ( depth === 'menu' && this.modifiers.length ) {
      cssClass = this.util._.uniq( cssClass.concat( this.modifiers ) )
    }

    let html = `<div class="${cssClass.join( ' ' )}" data-length="${items.length}"`
    if ( depth === 'menu' && this.id ) {
      html += ` data-id="${this.id}"`
    }
    html += '>'
    html += `<div class="${depth}__container">`
    if ( depth === 'menu' ) {
      html += '{{content}}'
    }
    html += `<ul class="${depth}__list">`
    items.forEach( item => {
      let itemClasses = [ `${depth}__item` ]
      let isActive = this.isActive( item )
      if ( isActive ) {
        itemClasses.push( isActive )
      }
      if ( item.slug ) {
        itemClasses.push( `${depth}__item--${item.slug}` )
      }

      html += `<li class="${itemClasses.join( ' ' )}" data-href="${item.url}"`
      if ( item.id ) {
        html += ` data-id="${item.id}"`
      }
      if ( item.attributes && item.attributes.length ) {
        item.attributes.forEach( attribute => {
          let key = Object.keys( attribute )[ 0 ]
          let value = Object.values( attribute )[ 0 ]
          html += ` ${key}="${value}"`
        } )
      }
      html += '>'

      let linkClasses = [ `${depth}__link` ]
      if ( item.items && item.items.length ) {
        linkClasses.push( 'has-submenu' )
      }

      html += `<a class="${linkClasses.join( ' ' )}" href="${item.url || ''}"`
      if ( item.external ) {
        html += ' target="_blank" rel="noopener"'
      }
      html += '>'
      html += `<span class="${depth}__text">${item.text || item.heading}</span>`
      html += '</a>'

      if ( item.items && item.items.length ) {
        if ( depth === 'menu' ) {
          html += '<span class="menu__toggle" role="button" aria-pressed="false">Toggle Submenu</span>'
          html += this.outputMenu( 'submenu', item.items )
        }
        if ( depth === 'submenu' ) {
          html += '<span class="submenu__toggle" role="button" aria-pressed="false">Toggle Subsubmenu</span>'
          html += this.outputMenu( 'subsubmenu', item.items )
        }
      }
    } )
    html += '</ul></div></div>'
    return html
  }

  outputEncoded() {
    let menu = {
      items: this.items.map( item => {
        return {
          external: item.external,
          id: item.id,
          items: item.items && item.items.map( _item => {
            return {
              external: _item.external,
              id: _item.id,
              link: _item.url,
              text: _item.text || _item.heading,
            }
          } ),
          link: item.url,
          text: item.text || item.heading,
        }
      } )
    }

    return this.util._.escape( JSON.stringify( menu ) )
  }

  // ------------------------------------------
  // Elements
  // ------------------------------------------
  get $content () {
    if ( this.$heading || this.$copy ) {
      return `<div class="menu__content">{{content}}</div>`
    }
  }

  get _content () {
    return `
    {{heading}}
    {{copy}}`
  }

  get $heading () {
    if ( this.heading ) {
      return `<h5 class="menu__heading">{{heading}}</h5>`
    }
  }

  get _heading () {
    return this.heading
  }

  get $copy () {
    if ( this.copy ) {
      return `<div class="menu__copy">{{copy}}</div>`
    }
  }

  get _copy () {
    return this.copy
  }
}

export default Menu
