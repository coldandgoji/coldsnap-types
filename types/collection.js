import Type from './_type.js'

export class Collection extends Type {
  get items () {
    return this.factory.wrap( this.data.items )
  }

  get links () {
    return this.factory.wrap( this.data.links, 'link' )
  }

  get link () {
    return this.cache( 'link', () => {
      let link = null
      if ( this.data.link ) {
        link = this.factory.make( this.data.link )
        if ( !link ) {
          link = this.data.link
        }
      }
      if ( !link && this.links ) {
        link = this.util._.get( this.links, '[0]', '' )
      }
      return link
    } )
  }

  // ----------------------------------------------
  // Template
  // ----------------------------------------------

  get $section () {
    if ( this.items.length ) {
      return super.$section
    }
  }

  get _container () {
    return `
      {{content}}
      {{list}}
      {{actions}}
      {{link}}`
  }

  get $content () {
    if ( this._heading || this._copy ) {
      return `<div class="collection__content">{{content}}</div>`
    }
  }

  get _content () {
    return `
      {{heading}}
      {{copy}}`
  }

  get $list () {
    return `<div class="collection__list">{{list}}</div>`
  }

  get _list () {
    return this.list( this.items, this.data.itemsMode )
  }

  get $actions () {
    if ( this.links && this.links.length ) {
      return `<div class="collection__actions">{{actions}}</div>`
    }
  }

  get _actions () {
    let html = ''
    this.links.forEach( link => {
      html += `<a class="collection__action" href="${link.url}">${link.text}</a>`
    } )
    return html
  }

  get _link () {
    if ( this.link && this.link.url ) {
      return `<a class="collection__link" href="${this.link.url}"></a>`
    }
  }

  /*
  get $cta () {
    if ( this.data.cta ) {
      return `<a class="collection__cta" href="${this.data.cta.url}">{{cta}}</a>`
    }
  }

  get _cta () {
    return this.data.cta.label
  }
  */
}

export default Collection
