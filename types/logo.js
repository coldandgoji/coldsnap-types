import Type from './_type.js'

export class Logo extends Type {
  // removes the background image from the logo
  get backgroundImageUrl () { }

  // ------------------------------------------
  // Template
  // ------------------------------------------

  get _container () {
    return `<a class="logo__link" href="${this.data.link}">{{figure}}</a>`
  }

  get _figure () {
    return `
      <figure class="logo__figure">
        {{image}}
        <figcaption class="logo__heading">${this.heading}</figcaption>
      </figure>`
  }

  get _image () {
    if ( this.data.image ) {
      if ( this.data.image.element ) {
        return this.data.image.element
      }
      if ( this.data.image.url ) {
        return `<img class="logo__image" src="${this.data.image.url}">`
      }
    }
  }
}

export default Logo
