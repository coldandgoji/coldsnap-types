import Type from './_type.js'

export class Site extends Type {
  get name () {
    return this.data.title
  }

  get themeColor () {
    return this.data.themeColor
  }

  get url () {
    return ( process.env.NODE_ENV === 'development' ) ? '/' : this.ssg.locale.domain || 'https://coldandgoji.com'
  }

  get logo () {
    return this.data.logo
  }

  get projectId () {
    return this.data.projectId
  }

  get title () {
    return this.data.title
  }

  get description () {
    return this.data.description
  }

  get partialLogo () {
    return {
      link: this.url,
      image: this.logo.image,
      heading: this.title
    }
  }
}

export default Site
