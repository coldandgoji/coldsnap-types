import Type from './_type.js'

export class Html extends Type {
  get $section () {
    if ( this.data.html ) {
      return this.data.html
    }
  }
}

export default Html
