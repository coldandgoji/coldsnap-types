import Mode from './_mode.js'

export default class Type extends Mode {
  constructor ( data, ssg ) {
    super( data, ssg )
    if ( new.target === Type ) {
      throw new TypeError( 'Cannot create Type instances directly' )
    }

    // starts with an empty cache
    this.cached = {}

    // builds the item data
    // taking the locale into account
    this.data = {}
    for ( let [ key, value ] of Object.entries( data ) ) {
      if ( value !== undefined && value !== null ) {
        this.data[ key ] = value
        if ( value[ this.ssg.locale.field ] !== undefined ) {
          this.data[ key ] = value[ this.ssg.locale.field ]
        } else {
          // this.ssg.log( 'Locale field \'' + this.ssg.locale.field + '\' not found on ' + data.id + ' : ' + key );
        }
      }
    }
  }

  // ------------------------------------------
  // Core
  // ------------------------------------------

  /**
   * Returns the 'content type' of this instance
   * This is determined from the class name
   */
  get contentType () {
    return this.cache( 'contentType', () => {
      let string = this.constructor.name
      return string.charAt( 0 ).toLowerCase() + string.slice( 1 )
    } )
  }

  /**
   * Determines if this content exists in the specified locale
   * This is used by the core to filter out items of data that
   * do not apply in particular locales
   */
  locale ( locale ) {
    return this.cache( 'locale_' + locale, () => {
      if ( this.data.locale && this.data.locale.length ) {
        return ( this.data.locale.indexOf( locale ) !== -1 )
      }
      return true
    } )
  }

  /**
   * Determines (and caches) a backref relationship
   */
  backref ( type, property ) {
    return this.cache( type + '-' + property, () => {
      return this.util._.filter( this.ssg.data[ type ], ( item ) => {
        return ( item[ property ].indexOf( this ) !== -1 )
      } )
    } )
  }

  /**
   * Executes the supplied function just once
   * caching the result and returning it for
   * all subsequent calls without re-computation
   */
  cache ( key, func ) {
    if ( !( key in this.cached ) ) {
      this.cached[ key ] = func()
    }
    return this.cached[ key ]
  }

  /**
   * Merges a set of objects
   * so that the values of later objects
   * overrides matching keys in earlier objects
   */
  merge ( ...params ) {
    return this.util._.assign( ...params )
  }

  // ------------------------------------------
  // Getters
  // ------------------------------------------

  get id () {
    return this.data.id
  }

  // TODO: remove pathName which is temporary handling
  // strips trailing slashes from the path
  // as long as the path isn't the home /
  get path () {
    return this.cache( 'path', () => {
      let path = this.data.path || this.data.pathName
      if ( path && path !== '/' ) {
        path = path.replace( new RegExp( '/$' ), '' )
      }
      return path
    } )
  }

  get name () {
    return this.cache( 'name', () => {
      return this.data.name || this.data.heading || ''
    } )
  }

  get heading () {
    return this.cache( 'heading', () => {
      return this.data.heading || this.data.name || ''
    } )
  }

  get slug () {
    return this.cache( 'slug', () => {
      return this.util.slug( this.data.slug || this.name || '' )
    } )
  }

  get summary () {
    try {
      return this.util.md( this.summaryOverride || this.data.summary || '' )
    } catch ( error ) {
      this.ssg.log( '' )
      this.ssg.log( 'ERROR: Summary was supplied incorrectly' )
      this.ssg.log( 'There may be a mismatch between the field locale and the locale in the data' )
      this.ssg.log( 'The requested locale is: ' + this.ssg.locale.field )
      this.ssg.log( 'The supplied data is:' )
      this.ssg.log( this.data.id )
      this.ssg.log( this.data.name )
      this.ssg.log( this.data.heading )
      this.ssg.log( this.data.summary )
      this.ssg.log( this.data.copy )
      console.log( error )
      process.exit( 1 )
    }
  }

  set summary ( text ) {
    this.summaryOverride = text
  }

  get copy () {
    try {
      return this.util.md( this.data.copy || '' )
    } catch ( error ) {
      this.ssg.log( '' )
      this.ssg.log( 'ERROR: Copy was supplied incorrectly' )
      this.ssg.log( 'There may be a mismatch between the field locale and the locale in the data' )
      this.ssg.log( 'The requested locale is: ' + this.ssg.locale.field )
      this.ssg.log( 'The supplied data is:' )
      this.ssg.log( this.data.id )
      this.ssg.log( this.data.name )
      this.ssg.log( this.data.heading )
      this.ssg.log( this.data.summary )
      this.ssg.log( this.data.copy )
      console.log( error )
      process.exit( 1 )
    }
  }

  get images () {
    // handles 'images' by default
    if ( this.data.images ) {
      return this.data.images
    }

    // falls back to 'image'
    // but returns it as an array so it can be
    // treated consistently in templates
    if ( this.data.image ) {
      return [ this.data.image ]
    }

    return []
  }

  /**
   * Attempts to determine an image for this item
   * from an 'image', 'images', 'banner' or 'banners' field
   * and falls back to 'defaultImage'
   */
  get image () {
    return this.cache( 'image', () => {
      let image = ''
      if ( this.data.image ) {
        image = this.data.image
      }
      if ( this.data.images && this.data.images.length ) {
        image = this.util._.get( this.data.images, '[0]', '' )
      }
      if ( !image && this.banners && this.banners.length ) {
        image = this.util._.get( this.banners, '[0]', '' )
      }
      if ( !image ) {
        image = this.defaultImage
      }
      return image
    } )
  }

  get imageUrl () {
    if ( this.image && this.image.url ) {
      return this.image.url
    }
  }

  get imageDescription () {
    if ( this.image && this.image.description ) {
      return this.image.description
    }
    return ''
  }

  get backgroundImageUrl () {
    return this.imageUrl
  }

  get defaultImage () {
    return null
  }

  get metaImage () {
    if ( this.image && this.image.url ) {
      let url = 'https:' + this.image.url
      if ( url.indexOf( 'ctfassets' ) !== -1 ) {
        url += '?fm=jpg&fit=fill&w=1200&h=630&f=faces&q=90'
      }
      return url
    }
    return this.site.url + '/media/images/logo.png'
  }

  get metaTitle () {
    return this.cache( 'metaTitle', () => {
      let metaTitle = ''

      // assigns the metaTitle from the data
      if ( !metaTitle ) {
        metaTitle = this.util.striptags( this.data.metaTitle )
      }

      // assigns the truncated heading
      if ( !metaTitle ) {
        metaTitle = this.util.striptags( this.heading ).substr( 0, 60 )
      }

      // assigns the site title
      if ( !metaTitle ) {
        metaTitle = this.util.striptags( this.site.title )
      }

      return metaTitle
    } )
  }

  get metaDescription () {
    return this.cache( 'metaDescription', () => {
      let metaDescription = ''
      // assigns the override
      // (this occurs when a type sets this.metaDescription in the constructor)
      if ( this.metaDescriptionOverride ) {
        metaDescription = this.util.striptags( this.metaDescriptionOverride )
      }
      // assigns the metaDescription from the data
      if ( !metaDescription ) {
        metaDescription = this.util.striptags( this.data.metaDescription )
      }
      // assigns the truncated copy
      if ( !metaDescription ) {
        metaDescription = this.util.striptags( this.copy ).substr( 0, 160 )
      }
      // assigns the site description
      if ( !metaDescription ) {
        metaDescription = this.util.striptags( this.site.description )
      }
      return metaDescription
    } )
  }

  set metaDescription ( text ) {
    this.metaDescriptionOverride = text
  }

  get banners () {
    let banners = this.data.banners
    if ( ( !banners || banners.length === 0 ) && this.data.banner ) {
      banners = [ this.data.banner ]
    }
    return this.factory.wrap( banners, 'banner' )
  }

  get banner () {
    let banner = null
    let banners = this.banners
    if ( banners.length ) {
      banner = banners[ 0 ]
    }
    if ( !banner && this.image && this.image.url ) {
      banner = this.component( 'banner', {
        heading: this.heading,
        image: this.image
      } )
    }
    return banner
  }

  get content () {
    let data = this.util._.clone( this.data )
    data.heading = this.heading
    data.summary = this.summary
    data.copy = this.copy
    let content = this.component( 'content', data )
    if ( content.heading || content.summary || content.copy ) {
      return content
    }
  }

  get modifiers () {
    let classes = [ this.cssContentType ]

    // automatically adds the mode as a modifier
    // but only when it isn't the current default of 'page'
    if ( this.$mode !== 'page' ) {
      classes.push( `${this.cssContentType}--${this.$mode}` )
    }

    // TODO: deprecate this
    // This still comes out of legacy data
    if ( this.data.modifier ) {
      classes.push( `${this.cssContentType}--${this.util.slug( this.data.modifier )}` )
    }

    if ( this.data.modifiers ) {
      classes = classes.concat( this.data.modifiers )
    }

    if ( !this.data.pattern && !this.data.modifier && !this.data.modifiers && this.$mode === 'page' ) {
      classes.push( `${this.cssContentType}--default` )
    }

    return this.util._.uniq( classes )
  }

  addModifier ( modifier ) {
    if ( !this.data.modifiers ) {
      this.data.modifiers = []
    }
    if ( !this.data.modifiers.includes( modifier ) ) {
      this.data.modifiers.push( modifier )
    }
  }

  removeModifier ( modifier ) {
    if ( !this.data.modifiers ) {
      this.data.modifiers = []
    }
    this.data.modifiers = this.util._.filter( this.data.modifiers, item => {
      return ( item !== modifier )
    } )
  }

  jsonEncode ( object ) {
    if ( typeof object !== 'string' ) {
      object = JSON.stringify( object )
    }
    return object.replace( /"/g, '&quot;' )
  }

  pluralize ( number, singular, plural ) {
    if ( number === 1 ) {
      return singular
    }
    return plural
  }

  removeWhitespace ( string ) {
    return string.replace( /(<(p|pre|script|style|textarea)[^]+?<\/\2)|(^|>)\s+|\s+(?=<|$)/g, '$1$3' )
  }

  // ----------------------------------------------------------

  get cssContentType () {
    return this.util._.kebabCase( this.contentType )
  }

  get _cssStyle () {
    let styles = []
    if ( this.backgroundImageUrl ) {
      styles.push( `background-image:url('${this.backgroundImageUrl}')` )
    }
    if ( styles.length ) {
      return ' style="' + styles.join( ';' ) + '"'
    }
  }

  get _dataCaption () {
    let text = this.imageDescription || ''
    return ` data-caption="${text}"`
  }

  get _carousel () {
    if ( this.banners ) {
      return this.component( 'carousel', { items: this.banners } )
    }
  }

  get _banner () {
    return this.banner
  }

  get _content () {
    return this.content
  }

  get _featuredContent () {
    return this.factory.wrap( this.data.featuredContent )
  }

  get _additionalContent () {
    return this.factory.wrap( this.data.additionalContent )
  }

  get $heading () {
    if ( this._heading ) {
      return `<div class="${this.cssContentType}__heading">{{heading}}</div>`
    }
  }

  get _heading () {
    return this.heading
  }

  get $summary () {
    if ( this.summary ) {
      return `<div class="${this.cssContentType}__summary">{{summary}}</div>`
    }
  }

  get _summary () {
    return this.summary
  }

  get $copy () {
    if ( this.copy ) {
      return `<div class="${this.cssContentType}__copy">{{copy}}</div>`
    }
  }

  get _copy () {
    return this.copy
  }
}
