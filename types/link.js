import Type from './_type.js'

export class Link extends Type {
  get text () {
    return this.data.text
  }

  get external () {
    return this.data.external
  }

  get url () {
    if ( this.reference && this.reference.url ) {
      return this.reference.url
    }
    return this.data.url
  }

  get reference () {
    return this.factory.make( this.data.reference )
  }

  get items () {
    return this.factory.wrap( this.data.items )
  }

  get attributes () {
    return this.data.attributes
  }
}

export default Link
