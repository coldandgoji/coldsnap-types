import Type from './_type.js'

export class Person extends Type {
  get url () {
    return '/person/' + this.slug
  }

  get firstName () {
    return this.data.firstName
  }

  get lastName () {
    return this.data.lastName
  }

  get fullName () {
    let html = ''
    if ( this.firstName ) {
      html += this.firstName
      if ( this.lastName ) {
        html += ' '
      }
    }
    if ( this.lastName ) {
      html += this.lastName
    }
    return html
  }

  get position () {
    return this.data.position
  }

  get organization () {
    return this.data.organization
  }

  get nationality () {
    return this.data.nationality
  }

  get email () {
    return this.data.email
  }

  get phone () {
    return this.data.phone
  }

  get facebook () {
    return this.data.facebook
  }

  get linkedIn () {
    return this.data.linkedIn
  }

  get twitter () {
    return this.data.twitter
  }

  get instagram () {
    return this.data.instagram
  }

  get biography () {
    return this.util.md( this.data.biography || '' )
  }

  get imageUrl () {
    return super.imageUrl + '?w=500&h=500&fit=fill&q=80'
  }

  get metaTitle () {
    return this.fullName
  }

  get metaDescription () {
    return this.biography.substr( 0, 160 )
  }

  // ----------------------------------------------
  // Template
  // ----------------------------------------------

  get _container () {
    return `
      {{figure}}
      {{content}}
      ${this.$mode === 'summary' ? '{{cta}}' : ''}`
  }

  get $figure () {
    if ( this.imageUrl || this.backgroundImageUrl ) {
      return `
        <figure class="person__figure" {{cssStyle}}>{{figure}}</figure>`
    }
  }

  get _figure () {
    return `
      ${this.$mode === 'summary' ? `<a class="person__link" href="${this.url}">` : ''}
      {{image}}
      {{figureCaption}}
      ${this.$mode === 'summary' ? '</a>' : ''}`
  }

  get _image () {
    if ( this.imageUrl ) {
      return `<img class="person__image" src="${this.imageUrl}" alt="${this.imageDescription}" />`
    }
  }

  get $figureCaption () {
    if ( this.fullName ) {
      return `<figcaption class="person__caption">{{figureCaption}}</figcaption>`
    }
  }

  get _figureCaption () {
    return this.fullName
  }

  get $content () {
    return `<div class="person__content">{{content}}</div>`
  }

  get _content () {
    return `
      {{name}}
      {{position}}
      {{nationality}}
      ${this.$mode !== 'summary' ? '{{biography}}' : ''}`
  }

  get $name () {
    if ( this.fullName ) {
      return `<h3 class="person__name">{{name}}</h3>`
    }
  }

  get _name () {
    return this.fullName
  }

  get $position () {
    if ( this.position ) {
      return `<h4 class="person__position">{{position}}</h4>`
    }
  }

  get _position () {
    return this.position
  }

  get $nationality () {
    if ( this.nationality ) {
      return `<span class="person__nationality" data-nationality="${this.nationality}">{{nationality}}</span>`
    }
  }

  get _nationality () {
    return this.nationality
  }

  get $biography () {
    return `<div class="person__biography">{{biography}}</div>`
  }

  get _biography () {
    return this.biography
  }

  get $cta () {
    return `<a class="person__cta" href="${this.url}">{{cta}}</a>`
  }

  get _cta () {
    return 'View Profile'
  }
}
export default Person
