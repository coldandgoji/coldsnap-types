import Page from './page.js'

export class FaqCategory extends Page {
  get url () {
    return '/faq/' + this.slug
  }

  get faqs () {
    return this.factory.wrap( this.data.faqs, 'faq' )
  }

  // ----------------------------------------------
  // Template
  // ----------------------------------------------

  /*
  get _toc () {
    return toc
  }
  */

  get $content () {
    return `<div class="faq-category__content">{{content}}</div>`
  }

  get _content () {
    let toc = '<div class="content__toc">'
    this.faqs.forEach( faq => {
      toc += `<a href="#${faq.slug}">${faq.question}</a><br>`
    } )
    toc += '</div>'
    this.data.copy = this.data.copy.replace( '{{toc}}', toc )
    return super._content
  }

  get _additionalContent () {
    let half = this.data.firstColumnCount
    if ( !half ) {
      half = Math.floor( this.faqs.length / 2 )
    }
    let firstHalf = this.faqs.slice( 0, half )
    let secondHalf = this.faqs.slice( half, this.faqs.length )

    let makeFaqCopy = ( faqs ) => {
      let listTypes = {
        'air-purifiers': 'air',
        'dehumidifiers': 'water'
      }
      let listType = listTypes[ this.slug ] || 'default'

      let copy = `<div class="list--${listType}"><ul>`
      faqs.forEach( faq => {
        copy += `<li><h5 id="${faq.slug}" class="faq__question">${faq.question}</h5>`
        copy += `<div class="faq__answer">${faq.answer}</div></li>`
      } )
      copy += '</ul></div>'
      return copy
    }

    let content = [
      this.component( 'collection.collection--variant-1', {
        items: [
          this.component( 'content.content--default.content--faq', {
            copy: makeFaqCopy( firstHalf )
          } ),
          this.component( 'content.content--default.content--faq', {
            copy: makeFaqCopy( secondHalf )
          } )
        ]
      } )
    ]

    if ( this.data.disclaimer ) {
      content.push(
        this.component( 'collection.collection--variant-2', {
          items: [
            this.component( 'content.content--default', {
              copy: this.util.md( this.data.disclaimer )
            } )
          ]
        } )
      )
    }

    return content
  }
}

export default FaqCategory
