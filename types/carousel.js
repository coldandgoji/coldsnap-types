import Type from './_type.js'

export class Carousel extends Type {
  get items () {
    return this.cache( 'items', () => {
      return this.factory.wrap( this.data.items )
    } )
  }

  // ------------------------------------------
  // Template
  // ------------------------------------------
  get $section () {
    if ( this.items.length === 1 ) {
      return this.items[0]
    }
    if ( this.items.length ) {
      return this.component( 'collection.collection--slider', {
        itemsMode: this.data.itemsMode,
        items: this.items
      } )
    }
    /*
    `
      <section class="collection collection--slider">
        <div class="collection__container">
          <ul class="collection__list">
            ${this.list( this.items, this.data.itemsMode )}
          </ul>
        </div>
      </section>
    `
    */
  }
}

export default Carousel
