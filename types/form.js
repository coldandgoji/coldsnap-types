import Type from './_type.js'

export class Form extends Type {
  // ------------------------------------------
  // Getters
  // ------------------------------------------
  /*
  get filter () {
    return this.data.filter || ''
  }
  */

  get modifiers () {
    let modifiers = super.modifiers
    if ( this.data.name ) {
      modifiers.push( `${this.cssContentType}--${this.data.name}` )
    }
    if ( this.data.active ) {
      modifiers.push( `is--active` )
    }
    return this.util._.uniq( modifiers )
  }

  // ------------------------------------------
  // Template
  // ------------------------------------------
  get $section () {
    return `
      <section{{cssClass}}{{dataFilter}}{{htmlId}}>
        {{container}}
      </section>`
  }

  get _dataFilter () {
    if ( this.data.filter ) {
      return ` data-filter="${this.data.filter}"`
    }
  }

  get _container () {
    return `
      {{close}}
      {{content}}
      {{messages}}
      {{form}}`
  }

  get $close () {
    return `<div class="form__close"></div>`
  }

  get $content () {
    return `<div class="form__content">{{content}}</div>`
  }

  get _content () {
    return `
      {{heading}}
      {{copy}}`
  }

  get $heading () {
    return `<h5 class="form__heading">{{heading}}</h5>`
  }

  get _heading () {
    return this.heading
  }

  get $copy () {
    return `<div class="form__copy">{{copy}}</div>`
  }

  get _copy () {
    return this.copy
  }

  get $messages () {
    return `<div class="form__messages"></div>`
  }

  /**
   * String builds the form due to the complex display logic
   */
  get _form () {
    let html = ''

    // Constructs opening form tag
    html += '<form class="form__form" accept-charset="UTF-8"'
    html += this.data.action ? ` action="${this.data.action}"` : ''
    html += this.data.error ? ` data-message-error="${this.data.error}"` : ''
    html += this.data.honeypot ? ` netlify-honeypot="hanipotto"` : ''
    html += this.data.method ? ` method="${this.data.method}"` : ''
    html += this.data.name ? ` name="${this.data.name}"` : ''
    html += this.data.netlify ? ` data-netlify="true"` : ''
    html += this.data.novalidate ? ' novalidate' : ''
    html += this.data.recaptcha ? ` data-netlify-recaptcha="true"` : ''
    html += this.data.success ? ` data-message-success="${this.data.success}"` : ''
    html += this.data.target ? ` target="${this.data.target}"` : ''
    html += this.request ? ` data-filter-request="${this.data.request}"` : ''
    html += '>'

    // Inserts honeypot
    if ( this.data.honeypot ) {
      html += `<input type="checkbox" name="hanipotto" />`
    }

    // loops through and builds each individual field
    if ( this.data.fields ) {
      if ( this.data.new ) {
        let fieldsetIndex = 0
        let fieldIndex = 0
        let inputIndex = 0

        for ( let field of this.data.fields ) {
          // Constructs fieldset
          if ( field.fieldset_start ) {
            fieldsetIndex++
            fieldIndex = 0
            let collapsed = !!field.collapsed
            let collapsible = !!field.collapsible
            let hidden = !!field.hidden

            let classes = ''
            classes += ` form__fieldset--${this.util.slug( field.legend, { lower: true } )}`
            classes += collapsed ? ' is--collapsed' : ''
            classes += collapsible ? ' js--collapsible' : ''
            classes += hidden ? ' is--hidden' : ''

            html += `<fieldset class="form__fieldset${classes}">`

            // Constructs legend
            if ( field.legend ) {
              let classes = ''
              classes += ` form__legend--${this.util.slug( field.legend, { lower: true } )}`

              html += `<legend class="form__legend${classes}">`
              html += field.legend
              html += '</legend>'
            }

            html += '<div class="form__fields">'
          }

          // Constructs field
          if ( field.label && field.type ) {
            fieldIndex++

            let disabled = !!field.disabled
            let hidden = !!field.hidden
            let id = field.id ? field.id : `${fieldsetIndex}-${fieldIndex}-${this.util.slug( field.label, { lower: true } )}`
            let name = field.name ? field.name : `${fieldsetIndex}-${fieldIndex}-${this.util.slug( field.label, { lower: true } )}`
            let pattern = field.pattern ? field.pattern : null
            let placeholder = field.placeholder ? field.placeholder : field.label
            let required = !!field.required
            if ( field.items && !required ) {
              required = field.items.some( ( item ) => {
                return item.required
              } )
            }
            let size = field.size ? ( parseInt( field.size.split( '/' )[ 0 ] ) / parseInt( field.size.split( '/' )[ 1 ] ) ) * 100 : 100
            let trigger = field.trigger ? field.trigger : ''
            let type = this.util.slug( field.type, { lower: true } )
            let value = field.value ? field.value : null

            let classes = ''
            classes += ` form__field--${type}`
            classes += hidden ? ' is--hidden' : ''
            classes += required ? ' form__field--required' : ''

            html += `<div class="form__field${classes}"`
            html += trigger ? ` data-trigger="${trigger}"` : ''
            html += size ? ` style="width: calc( ${size}% - 2em );"` : ''
            html += '>'

            // Constructs input
            switch ( field.type ) {
              case 'button':
                html += '<button class="form__input"'
                html += disabled ? ' disabled' : ''
                html += id ? ` id="${id}"` : ''
                html += name ? ` name="${name}"` : ''
                html += type ? ` type="${type}"` : ''
                html += '>'
                html += field.label
                html += '</button>'
                break

              case 'checkbox':
                inputIndex = 0

                for ( let item of field.items ) {
                  inputIndex++
                  if ( !item.id ) item.id = `${fieldsetIndex}-${fieldIndex}-${inputIndex}-${this.util.slug( field.label, { lower: true } )}`
                  if ( !item.name ) item.name = `${fieldsetIndex}-${fieldIndex}-${this.util.slug( field.label, { lower: true } )}[]`

                  // Constructs checkbox
                  html += '<input class="form__input"'
                  html += type ? ` type="${type}"` : ''
                  html += item.id ? ` id="${item.id}"` : ''
                  html += item.name ? ` name="${item.name}"` : ''
                  html += item.checked ? ' checked' : ''
                  html += item.disabled ? ' disabled' : ''
                  html += item.required ? ' required' : ''
                  html += item.selected ? ' selected' : ''
                  html += item.trigger ? ' data-trigger' : ''
                  html += item.value || item.value === '' ? ` value="${item.value}"` : ` value="${item.label}"`
                  html += ' />'

                  // Constructs checkbox label
                  html += '<label class="form__label form__label--inline"'
                  html += ` for="${item.id}"`
                  html += '>'
                  html += item.label
                  html += '</label>'
                }
                break

              case 'color':
                html += '<p>The <em>color</em> input type is not yet supported by Coldsnap</p>'
                break

              case 'date':
                html += '<input class="form__input"'
                html += disabled ? ' disabled' : ''
                html += id ? ` id="${id}"` : ''
                html += name ? ` name="${name}"` : ''
                html += pattern ? ` pattern="${pattern}"` : ''
                html += placeholder ? ` placeholder="${placeholder}"` : ''
                html += required ? ' required' : ''
                html += type ? ` type="${type}"` : ''
                html += value ? ` value="${value}"` : ''
                html += ' />'
                break

              case 'datetime-local':
                html += '<p>The <em>datetime-local</em> input type is not yet supported by Coldsnap</p>'
                break

              case 'email':
                html += '<input class="form__input"'
                html += disabled ? ' disabled' : ''
                html += id ? ` id="${id}"` : ''
                html += name ? ` name="${name}"` : ''
                html += pattern ? ` pattern="${pattern}"` : ''
                html += placeholder ? ` placeholder="${placeholder}"` : ''
                html += required ? ' required' : ''
                html += type ? ` type="${type}"` : ''
                html += value ? ` value="${value}"` : ''
                html += ' />'
                break

              case 'file':
                html += '<p>The <em>file</em> input type is not yet supported by Coldsnap</p>'
                break

              case 'hidden':
                html += '<input class="form__input"'
                html += disabled ? ' disabled' : ''
                html += id ? ` id="${id}"` : ''
                html += name ? ` name="${name}"` : ''
                html += pattern ? ` pattern="${pattern}"` : ''
                html += placeholder ? ` placeholder="${placeholder}"` : ''
                html += required ? ' required' : ''
                html += type ? ` type="${type}"` : ''
                html += value ? ` value="${value}"` : ''
                html += ' />'
                break

              case 'image':
                html += '<p>The <em>image</em> input type is not yet supported by Coldsnap</p>'
                break

              case 'month':
                html += '<p>The <em>month</em> input type is not yet supported by Coldsnap</p>'
                break

              case 'number':
                html += '<p>The <em>number</em> input type is not yet supported by Coldsnap</p>'
                break

              case 'password':
                html += '<input class="form__input"'
                html += disabled ? ' disabled' : ''
                html += id ? ` id="${id}"` : ''
                html += name ? ` name="${name}"` : ''
                html += pattern ? ` pattern="${pattern}"` : ''
                html += placeholder ? ` placeholder="${placeholder}"` : ''
                html += required ? ' required' : ''
                html += type ? ` type="${type}"` : ''
                html += value ? ` value="${value}"` : ''
                html += ' />'
                break

              case 'radio':
                inputIndex = 0

                for ( let item of field.items ) {
                  inputIndex++
                  if ( !item.id ) item.id = `${fieldsetIndex}-${fieldIndex}-${inputIndex}-${this.util.slug( field.label, { lower: true } )}`
                  if ( !item.name ) item.name = `${fieldsetIndex}-${fieldIndex}-${this.util.slug( field.label, { lower: true } )}[]`

                  // Constructs checkbox
                  html += '<input class="form__input"'
                  html += type ? ` type="${type}"` : ''
                  html += item.id ? ` id="${item.id}"` : ''
                  html += item.name ? ` name="${item.name}"` : ''
                  html += item.checked ? ' checked' : ''
                  html += item.disabled ? ' disabled' : ''
                  html += item.required ? ' required' : ''
                  html += item.selected ? ' selected' : ''
                  html += item.trigger ? ' data-trigger' : ''
                  html += item.value || item.value === '' ? ` value="${item.value}"` : ` value="${item.label}"`
                  html += ' />'

                  // Constructs checkbox label
                  html += '<label class="form__label form__label--inline"'
                  html += ` for="${item.id}"`
                  html += '>'
                  html += item.label
                  html += '</label>'
                }
                break

              case 'range':
                html += '<p>The <em>range</em> input type is not yet supported by Coldsnap</p>'
                break

              case 'recaptcha':
                html += '<div data-netlify-recaptcha="true"></div>'
                break

              case 'reset':
                html += '<p>The <em>reset</em> input type is not yet supported by Coldsnap</p>'
                break

              case 'search':
                html += '<p>The <em>search</em> input type is not yet supported by Coldsnap</p>'
                break

              case 'select':
                html += '<select class="form__input"'
                html += disabled ? ' disabled' : ''
                html += id ? ` id="${id}"` : ''
                html += name ? ` name="${name}"` : ''
                html += required ? ' required' : ''
                html += type ? ` type="${type}"` : ''
                html += '>'

                for ( let item of field.items ) {
                  html += '<option class="form__option"'
                  html += item.disabled ? ' disabled' : ''
                  html += item.selected ? ' selected' : ''
                  html += item.trigger ? ' data-trigger' : ''
                  html += item.value || item.value === '' ? ` value="${item.value}"` : ` value="${item.label}"`
                  html += '>'
                  html += item.label
                  html += '</option>'
                }

                html += '</select>'
                break

              case 'submit':
                html += '<button class="form__input"'
                html += id ? ` id="${id}"` : ''
                html += name ? ` name="${name}"` : ''
                html += type ? ` type="${type}"` : ''
                html += '>'
                html += field.label
                html += '</button>'
                break

              case 'tel':
                html += '<input class="form__input"'
                html += disabled ? ' disabled' : ''
                html += id ? ` id="${id}"` : ''
                html += name ? ` name="${name}"` : ''
                html += pattern ? ` pattern="${pattern}"` : ''
                html += placeholder ? ` placeholder="${placeholder}"` : ''
                html += required ? ' required' : ''
                html += type ? ` type="${type}"` : ''
                html += value ? ` type="${value}"` : ''
                html += ' />'
                break

              case 'text':
                html += '<input class="form__input"'
                html += disabled ? ' disabled' : ''
                html += id ? ` id="${id}"` : ''
                html += name ? ` name="${name}"` : ''
                html += pattern ? ` pattern="${pattern}"` : ''
                html += placeholder ? ` placeholder="${placeholder}"` : ''
                html += required ? ' required' : ''
                html += type ? ` type="${type}"` : ''
                html += value ? ` value="${value}"` : ''
                html += ' />'
                break

              case 'textarea':
                html += '<textarea class="form__input"'
                html += disabled ? ' disabled' : ''
                html += id ? ` id="${id}"` : ''
                html += name ? ` name="${name}"` : ''
                html += pattern ? ` pattern="${pattern}"` : ''
                html += placeholder ? ` placeholder="${placeholder}"` : ''
                html += required ? ' required' : ''
                html += '>'
                html += value || ''
                html += '</textarea>'
                break

              case 'time':
                html += '<p>The <em>time</em> input type is not yet supported by Coldsnap</p>'
                break

              case 'url':
                html += '<p>The <em>url</em> input type is not yet supported by Coldsnap</p>'
                break

              case 'week':
                html += '<p>The <em>week</em> input type is not yet supported by Coldsnap</p>'
                break
            }

            // Constructs label
            html += '<label class="form__label"'
            html += ` for="${id}"`
            html += '>'
            html += field.label
            html += '</label>'

            html += '</div>'
          }

          // closes the fieldset
          if ( field.fieldset_end ) {
            html += '</div>'
            html += '</fieldset>'
          }
        }
      } else {
        for ( let field of this.data.fields ) {
          let maxLength
          let value
          let matchingOption

          // adds a fieldset start
          if ( field.fieldset_start ) {
            let fieldsetClasses
            field.legend ? fieldsetClasses = ` fieldset--${field.legend.toLowerCase()}` : fieldsetClasses = ''
            html += `<fieldset class="form__fieldset${fieldsetClasses}">`
          }

          // adds a legend
          if ( field.legend ) {
            html += `<legend class="form__legend">`
            html += field.legend
            html += '</legend>'
          }

          // adds a field start
          let fieldType = field.type ? this.util.slug( field.type.toLowerCase() ) : ''
          let fieldName = field.name ? this.util.slug( field.name.toLowerCase() ) : ''
          html += '<div class="form__field'
          if ( fieldType ) {
            html += ` form__field--${fieldType}`
          }
          if ( fieldName && fieldName !== fieldType ) {
            html += ` form__field--${fieldName}`
          }
          if ( field.parent ) {
            html += ' form__field--child'
          }
          if ( field.found ) {
            html += ' form__field--' + field.found
          }
          if ( field.protected ) {
            html += ' form__field--protected'
          }
          html += '">'

          // adds a pre-label
          if ( field.preLabel ) {
            html += `<label class="form__label" for="${field.id}">`
            html += field.preLabel
            html += '</label>'
          }

          // adds the field based on type
          switch ( field.type ) {
            // adds a hidden field
            case 'hidden':
              html += `<input class="form__input" type="hidden" name="${field.name}" value="${field.value}" />`
              break

            // adds a text field
            case 'text':
              html += `<input class="form__input" type="${field.type}" name="${field.name}" placeholder="${field.placeholder}"`
              if ( field.pattern ) {
                html += ` pattern="${field.pattern}"`
              }
              if ( field.required ) {
                html += ' required'
              }
              html += ' />'
              break

            // adds a quantity field
            case 'quantity':
              maxLength = field.max_length || 2
              value = field.value || 1
              html += `<input class="form__input form__input--${field.type}" type="text" name="${field.name}" maxlength="${maxLength}" value="${value}"`
              if ( field.pattern ) {
                html += ` pattern="${field.pattern}"`
              }
              if ( field.required ) {
                html += ' required'
              }
              html += ' />'
              break

            // adds a password field
            case 'password':
              html += `<input class="form__input form__input--${field.type}" type="${field.type}" name="${field.name}" placeholder="${field.placeholder}"`
              if ( field.pattern ) {
                html += ` pattern="${field.pattern}"`
              }
              if ( field.required ) {
                html += ' required'
              }
              html += ' />'
              break

            // adds an email field
            case 'email':
              html += `<input class="form__input form__input--${field.type}" type="${field.type}" name="${field.name}" placeholder="${field.placeholder}"`
              if ( field.pattern ) {
                html += ` pattern="${field.pattern}"`
              }
              if ( field.required ) {
                html += ' required'
              }
              html += ' />'
              break

            // adds a tel field
            case 'tel':
              html += `<input class="form__input form__input--${field.type}" type="${field.type}" name="${field.name}" placeholder="${field.placeholder}"`
              if ( field.pattern ) {
                html += ` pattern="${field.pattern}"`
              }
              if ( field.required ) {
                html += ' required'
              }
              html += ' />'
              break

            // adds a textarea
            case 'textarea':
              html += `<textarea class="form__input form__input--${field.type}" name="${field.name}" placeholder="${field.placeholder}"`
              if ( field.pattern ) {
                html += ` pattern="${field.pattern}"`
              }
              if ( field.required ) {
                html += ' required'
              }
              html += '></textarea>'
              break

            // adds a checkbox
            case 'checkbox':
              html += `<input class="form__input form__input--${field.type}" type="${field.type}" value="${field.value}" name="${field.name}" id="${field.id}"`
              if ( field.checked ) {
                html += ' checked'
              }
              if ( field.required ) {
                html += ' required'
              }
              if ( field.disabled ) {
                html += ' disabled'
              }
              if ( field.parent ) {
                html += ` data-parent-input="${field.parent}"`
              }
              html += ' />'
              break

            case 'radio':
              html += `<input class="form__input form__input--${field.type}" type="${field.type}" value="${field.value}" name="${field.name}" id="${field.id}"`
              if ( field.required ) {
                html += ' required'
              }
              if ( field.parent ) {
                html += ` data-parent-input="${field.parent}"`
              }
              html += ' />'
              break

            case 'select':
              html += `<select class="form__input form__input--${field.type}" type="${field.type}"`
              if ( field.name ) {
                html += `name="${field.name}"`
              }
              if ( field.required ) {
                html += ' required'
              }
              html += '>'

              field.options.forEach( ( option ) => {
                html += `<option class="form__option" label="${option.label}"`
                if ( option.value || option.value === '' ) {
                  html += ` value="${option.value}"`
                } else {
                  html += ` value="${option.label}"`
                }
                if ( option.selected ) {
                  html += ' selected'
                }
                if ( option.disabled ) {
                  html += ' disabled'
                }
                if ( option.parent ) {
                  html += ` data-parent-option="${option.parent}"`
                }
                html += '>'
                html += option.label
                html += '</option>'
              } )

              html += '</select>'
              break

            case 'product':
              matchingOption = field.matching_option || ''
              html += '<select class="form__select form__select--product"'
              if ( field.required ) {
                html += ' required'
              }
              html += '>'

              field.options.forEach( ( option ) => {
                html += `<option value="{ option.value }" data-matching-option="${matchingOption}">${option.name}</option>`
              } )

              html += '</select>'
              break

            case 'link':
              html += `<a class="form__link" href="${field.url}">${field.text}</a>`
              break

            case 'submit':
              html += `<button class="form__submit" type="${field.type}" name="${field.name}">${field.placeholder}</button>`
              break
          }

          if ( field.label ) {
            html += '<label class="form__label"'
            html += field.id ? `for="${field.id}"` : ''
            html += `>${field.label}</label>`
          }

          // closes the field
          html += '</div>'

          // closes the fieldset
          if ( field.fieldset_end ) {
            html += '</fieldset>'
          }
        }
      }
    }

    // closes the form
    html += '</form>'

    // returns the html
    return html
  }
}

export default Form
