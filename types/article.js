import Type from './_type.js'

export default class Article extends Type {
  get url () {
    return '/article/' + this.slug
  }

  get publicationDate () {
    return this.data.publicationDate
  }

  // TODO: Check if we should be accessing via this or 'publicationData'
  get date () {
    console.log( ' ' )
    console.log( 'DO NOT ACCESS ARTICLE DATE - USE PUBLICATION DATE' )
    console.log( ' ' )
    process.exit( 1 )
  }

  get dateFormatted () {
    return this.util.formatDate( this.publicationDate, 'MMMM Do' )
  }

  get articleSection () {
    return this.factory.make( this.data.section, 'articleSection' )
  }

  get formattedSummary () {
    if ( this.data.summary ) {
      return this.data.summary
    } else if ( this.data.copy ) {
      return ( this.util.striptags( this.data.copy ) ).split( ' ' ).splice( 0, 50 ).join( ' ' )
    } else {
      return ''
    }
  }

  get authors () {
    return this.factory.wrap( this.data.authors, 'person' )
  }

  get categories () {
    return this.factory.wrap( this.data.categories, 'articleCategory' )
  }

  get tags () {
    return this.factory.wrap( this.data.tags, 'articleTag' )
  }

  authorImageUrl ( person ) {
    if ( person.image?.url ) {
      return person.image.url + '?w=100&h=100&fit=fill&q=80'
    }
  }

  // ------------------------------------------
  // Template
  // ------------------------------------------

  get _container () {
    if ( this.$mode === 'summary' ) {
      return `
        {{main}}
        {{cta}}`
    }
    return `
      {{main}}
      {{aside}}`
  }

  get $main () {
    return `<div class="article__main">{{main}}</div>`
  }

  get _main () {
    return `
      {{figure}}
      {{content}}`
  }

  get $content () {
    return `<div class="article__content">{{content}}</div>`
  }

  get _content () {
    return `
      {{heading}}
      {{meta}}
      {{copy}}`
  }

  get $aside () {
    return `<div class="article__aside">{{aside}}</div>`
  }

  get _aside () {
    return `
      {{articleSection}}
      {{articleCategories}}
      {{articleTags}}`
  }

  get $figure () {
    if ( this.imageUrl || this.backgroundImageUrl ) {
      return `<figure class="article__figure"{{cssStyle}}{{dataCaption}}>{{figure}}</figure>`
    }
  }

  get _figure () {
    return `
      {{image}}
      <figcaption class="article__caption"></figcaption>`
  }

  get _image () {
    if ( this.imageUrl ) {
      return `<img class="article__image" src="${this.imageUrl}" alt="${this.imageDescription}" />`
    }
  }

  get $heading () {
    if ( this.heading ) {
      if ( this.$mode === 'summary' ) {
        return '<h3 class="article__heading">{{heading}}</h3>'
      }
      return '<h1 class="article__heading">{{heading}}</h1>'
    }
  }

  get _heading () {
    return this.heading
  }

  get $meta () {
    return `<div class="article__meta">{{meta}}</div>`
  }

  get _meta () {
    return `
      {{authors}}
      {{time}}`
  }

  get $copy () {
    if ( ( this.$mode === 'summary' && this.summary ) || ( this.$mode !== 'summary' && this.copy ) ) {
      return `<div class="article__copy">{{copy}}</div>`
    }
  }

  get _copy () {
    if ( this.$mode === 'summary' && this.summary ) {
      return this.summary
    } else if ( this.copy ) {
      return this.copy
    }
  }

  get $authors () {
    return `<ul class="article__authors">{{authors}}</ul>`
  }

  get _authors () {
    let html = ''
    this.authors.forEach( person => {
      html += '<li class="article__author">'
      html += `<img class="article__author-image" src="${this.authorImageUrl( person )}" alt="${person.fullName}" />`
      html += `<span class="article__author-name">${person.fullName}</span>`
      html += '</li>'
    } )
    return html
  }

  get $time () {
    return `<time class="article__time">{{time}}</time>`
  }

  get _time () {
    return this.dateFormatted
  }

  get $articleSection () {
    if ( this.articleSection ) {
      return `<a class="article__section" href="${this.articleSection.url}">${this.articleSection.heading}</a>`
    }
  }

  get $articleCategories () {
    if ( this.categories.length ) {
      return `<div class="article__categories">{{articleCategories}}</div>`
    }
  }

  get _articleCategories () {
    let html = ''
    this.categories.forEach( category => {
      html += `<a class="article__category" href="${category.url( this.articleSection )}">${category.heading}</a>`
    } )
    return html
  }

  get $articleTags () {
    if ( this.tags.length ) {
      return `<div class="article__tags">{{articleTags}}</div>`
    }
  }

  get _articleTags () {
    let html = ''
    this.tags.forEach( tag => {
      html += `<a class="article__tag" href="${tag.url( this.articleSection )}">${tag.heading}</a>`
    } )
    return html
  }

  get $cta () {
    return `<a class="article__cta" href="${this.url}">{{cta}}</a>`
  }

  get _cta () {
    return 'Read Article'
  }
}
