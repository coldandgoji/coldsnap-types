import Page from './page.js'

export class PageHome extends Page {
  // ----------------------------------------------
  // Template
  // ----------------------------------------------

  get _container () {
    return `
      {{carousel}}
      {{additionalContent}}`
  }
}

export default PageHome
