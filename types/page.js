import Type from './_type.js'

export class Page extends Type {
  get url () {
    return this.path
  }

  // ----------------------------------------------
  // Template
  // ----------------------------------------------

  get _container () {
    return `
      {{page}}
      {{additionalContent}}`
  }

  get _page () {
    if ( this._banner || this._content ) {
      return this.component( 'layout.layout--page', {
        items: [
          this._banner,
          this._content
        ]
      } )
    }
  }
}

export default Page
