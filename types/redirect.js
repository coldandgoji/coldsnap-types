import Type from './_type.js'

export class Redirect extends Type {
  get destination () {
    return this.data.destination
  }
}

export default Redirect
