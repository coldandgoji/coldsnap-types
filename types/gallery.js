import Type from './_type.js'

export class Gallery extends Type {
  // ----------------------------------------------
  // Template
  // ----------------------------------------------

  get $section () {
    if ( this.data.items && this.data.items.length ) {
      return super.$section
    }
  }

  get _container () {
    return `
      {{content}}
      {{list}}`
  }

  get $content () {
    if ( this._heading ) {
      return `<div class="gallery__content">{{content}}</div>`
    }
  }

  get _content () {
    return `{{heading}}`
  }

  get $heading () {
    return `<h3 class="gallery__heading">{{heading}}</h3>`
  }

  get $list () {
    return `<div class="gallery__list">{{list}}</div>`
  }

  get _list () {
    let images = this.data.items.map( image => {
      return this.component( 'html', { html:
        `<figure class="gallery__figure" style="background-image:url('${image.url}?w=300&h=300&fit=fill&q=80');" data-image="${image.url}" data-caption="${image.description}">
          <img class="gallery__image" src="${image.url}?w=300&h=300&fit=fill&q=80" alt="" />
          <figcaption class="gallery__caption">${image.description}</figcaption>
        </figure>`
      } )
    } )
    return this.list( images, this.data.itemsMode )
  }
}

export default Gallery
